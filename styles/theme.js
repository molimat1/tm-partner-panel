import { createTheme, responsiveFontSizes } from "@material-ui/core/styles";

let theme = createTheme({
  palette: {
    primary: {
      main: "#222",
    },
    secondary: {
      main: "#0c9",
    },
  },
});

theme = responsiveFontSizes(theme);

export default theme;
