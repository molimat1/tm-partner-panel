import {
    drawerWidth,
    transition,
    container,
} from "./nextjs-material-dashboard.js";

const appStyle = (theme) => ({
    wrapper: {
        position: "relative",
        top: "0",
        paddingTop: "65px",
        height: "100vh",
        backgroundColor: "#333",
    },
    mainPanel: {
        [theme.breakpoints.up("md")]: {
            width: `100%`,
        },
        overflow: "auto",
        position: "relative",
        float: "right",
        ...transition,
        maxHeight: "100%",
        width: "100%",
        overflowScrolling: "touch",
    },
});

export default appStyle;
