import React, { useEffect } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import NativeSelect from "@material-ui/core/NativeSelect";
import InputBase from "@material-ui/core/InputBase";
import { Typography } from "@material-ui/core";

const monthArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

const yearArray = [2021, 2022, 2023];

function MonthPicker({ onChange }) {
  const [month, setMonth] = React.useState(new Date().getMonth());
  const [year, setYear] = React.useState(new Date().getFullYear());

  useEffect(() => {
    onChange({ month, year });
  }, [month, year]);

  return (
    <div className="edit">
      <FormControl>
        <Typography style={{ color: "white" }}>Mês</Typography>
        <NativeSelect
          labelId="demo-controlled-open-select-label"
          id="demo-controlled-open-select"
          value={month}
          variant="outlined"
          style={{
            backgroundColor: "white",
          }}
          InputProps={{
            style: {
              color: "red",
            },
          }}
          onChange={(e) => {
            setMonth(parseInt(e.target.value));
          }}
        >
          {monthArray.map((item, key) => (
            <option value={key + 1} key={key}>
              {item}
            </option>
          ))}
        </NativeSelect>

        <NativeSelect
          labelId="year-open-select-label"
          id="year-open-select"
          style={{
            backgroundColor: "white",
            marginTop: 20,
          }}
          InputProps={{
            style: {
              color: "red",
            },
          }}
          variant="outlined"
          value={year}
          onChange={(e) => {
            setYear(parseInt(e.target.value));
          }}
        >
          {yearArray.map((item, key) => (
            <option value={item} key={key}>
              {item}
            </option>
          ))}
        </NativeSelect>
      </FormControl>
    </div>
  );
}

export default MonthPicker;
