import React from "react";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableHeading from "./TableHeading";
import TableItem from "./TableInvoiceItem";
import Box from "@material-ui/core/Box";

const RecentTable = ({ tableData, columns }) => {
    return (
        <Box className="Cmt-table-responsive">
            <Table>
                <TableHead>
                    <TableHeading data={columns} />
                </TableHead>
                <TableBody>
                    {tableData?.map((row, index) => (
                        <TableItem row={row} key={index} columns={columns} />
                    ))}
                </TableBody>
            </Table>
        </Box>
    );
};

export default RecentTable;
