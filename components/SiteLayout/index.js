import React, { useEffect } from "react";
import { useRouter } from "next/router";
// creates a beautiful scrollbar
// import PerfectScrollbar from "react-perfect-scrollbar";
// import 'react-perfect-scrollbar/dist/css/styles.css';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import useSWR from "swr";

// core components

import styles from "../../styles/layoutStyle.js";

import { useUser } from "../hooks/UserContext";
import { useGlobalData } from "../hooks/DataContext";
import Unauthorized from "../ui/Unauthorized";
import Loading from "../ui/Loading";
import Navbar from "../Navbar";

let ps;

const nextDataFetcher = (url, partnerCode) =>
  fetch(`${url}`, {
    headers: { Accept: "application/json", token: partnerCode },
  }).then((response) => response.json());

const SiteLayout = ({ children, ...rest }) => {
  // used for checking current route
  const router = useRouter();
  const { session, loading, userDetails, signOut } = useUser();
  const { setInvoices, setCoupons, setCustomers } = useGlobalData();
  // styles

  const useStyles = makeStyles(styles);
  const classes = useStyles();
  // ref to help us initialize PerfectScrollbar on windows devices
  const mainPanel = React.createRef();
  // states and functions
  const [update, setUpdate] = React.useState(false);
  const [shouldFetch, setShouldFetch] = React.useState(false);
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  useEffect(() => {
    if (userDetails) {
      if (userDetails[0].coupon && !shouldFetch) setShouldFetch(true);
    }
    if (!session && !loading) router.push("/");
  }, [userDetails, router, session]);

  if (!session && !loading) {
    return <Unauthorized />;
  }
  if (loading || !userDetails) {
    return <Loading message="Logando..." />;
  }

  return (
    <div>
      <CssBaseline />
      <div className={classes.wrapper}>
        <div className={classes.mainPanel} ref={mainPanel}>
          <Navbar
            handleDrawerToggle={handleDrawerToggle}
            signOut={signOut}
            user={userDetails[0]}
            {...rest}
          />
          <div className={classes.map}>{children}</div>
        </div>
      </div>
    </div>
  );
};

export const getLayout = (page) => <SiteLayout>{page}</SiteLayout>;

export default SiteLayout;
