import React, { useState, useEffect } from "react";

import axios from "axios";
import {
    FormControl,
    TextField,
    Button,
    Typography,
    Grid,
    Link,
    LinearProgress,
} from "@material-ui/core";

import { makeStyles, withStyles } from "@material-ui/core/styles";
import { supabase } from "../utils/initSupabase";
import { useRouter } from "next/router";
import { useUser } from "./hooks/UserContext";

const useStyles = makeStyles((theme) => ({
    form: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
    },
    submit: {
        margin: theme.spacing(4, 0, 4),
        backgroundColor: "#0c9",
        color: "#fcfcfc",
        "&:hover": {
            color: "#222",
        },
        transition: "all .4s",
    },
    textField: {
        width: "20vw",
        [theme.breakpoints.down("sm")]: {
            width: "75vw",
        },
        marginLeft: "auto",
        marginRight: "auto",
        paddingBottom: 0,
        marginTop: 20,
        fontWeight: 500,
    },
    input: {
        backgroundColor: "#ececec",
        color: "#000",
        "&:hover": {
            color: "#fff",
        },
    },
}));

const initialFValues = {
    email: "",
    password: "",
    coupon: "",
    name: "",
};

const ValidationTextField = withStyles({
    root: {
        "& .MuiFormLabel-root.Mui-focused": {
            color: "#0c9",
        },

        "& .MuiFilledInput-underline::after": {
            borderBottom: "2px solid #0c9",
        },
        "& .Mui-focused": {
            color: "#fff",
        },
    },
})(TextField);

const LoginFormFields = () => {
    const { user, signUp } = useUser();

    const [values, setValues] = useState(initialFValues);
    const [errors, setErrors] = useState({});
    const [message, setMessage] = useState("");
    const [color, setColor] = useState("#333");
    const [loading, setLoading] = useState(false);

    const router = useRouter();

    const createUser = async () => {
        setLoading(true);
        setMessage("");

        const { error, user } = await signUp({
            email: values.email.toLowerCase(),
            password: values.password,
        });

        if (error) {
            setColor("#E3170A");
            setMessage("Problema na criação: ", error.message);
            console.log(error);
        } else {
            setColor("#5Aa684");
            if (user) {
                const name = values.name;
                const coupon = values.coupon;
                const email = values.email;

                await fetch("/api/updateUser", {
                    method: "POST",
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify({
                        data: {
                            full_name: name,
                            coupon: coupon,
                            email: email,
                        },
                        token: user.id,
                    }),
                });
            }

            setMessage("Usuário criado.");
            resetForm();
        }
        setLoading(false);
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setValues({
            ...values,
            [name]: value,
        });
    };

    const resetForm = () => {
        setValues(initialFValues);
        setErrors({});
    };

    const validate = (fieldValues = values) => {
        let temp = { ...errors };

        if ("email" in fieldValues) {
            temp.email = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
                fieldValues.email
            )
                ? ""
                : "Por favor, digite seu e-mail.";
        }
        if ("password" in fieldValues) {
            temp.password =
                fieldValues.password.length > 7
                    ? ""
                    : "Sua senha deve ter pelo menos 8 caracteres.";
        }
        if ("name" in fieldValues) {
            temp.name =
                fieldValues.name.length > 3
                    ? ""
                    : "Por favor, preencha o nome completo.";
        }

        setErrors({
            ...temp,
        });

        if (fieldValues === values)
            return Object.values(temp).every((x) => x == "");
    };

    const handleSubmit = React.useCallback(async (e) => {
        e.preventDefault();
        if (validate()) {
            setMessage("Acessando...");
            createUser();
        }
    });

    const classes = useStyles();
    return (
        <FormControl>
            <form className={classes.form} noValidate onSubmit={handleSubmit}>
                <ValidationTextField
                    variant="filled"
                    margin="normal"
                    required
                    value={values.name}
                    onChange={handleInputChange}
                    autoFocus
                    id="name"
                    label="Nome"
                    name="name"
                    autoComplete="name"
                    error={errors.name ? true : false}
                    helperText={errors.name}
                    disabled={loading}
                    className={classes.textField}
                    InputProps={{
                        className: classes.input,
                    }}
                />
                <ValidationTextField
                    variant="filled"
                    margin="normal"
                    required
                    value={values.email}
                    onChange={handleInputChange}
                    autoFocus
                    id="email"
                    label="E-mail"
                    name="email"
                    autoComplete="email"
                    error={errors.email ? true : false}
                    helperText={errors.email}
                    disabled={loading}
                    className={classes.textField}
                    InputProps={{
                        className: classes.input,
                    }}
                />
                <ValidationTextField
                    variant="filled"
                    margin="normal"
                    required
                    fullWidth
                    value={values.password}
                    onChange={handleInputChange}
                    name="password"
                    label="Senha"
                    id="password"
                    type="text"
                    error={errors.password ? true : false}
                    helperText={errors.password}
                    disabled={loading}
                    className={classes.textField}
                    InputProps={{
                        className: classes.input,
                    }}
                />
                <ValidationTextField
                    variant="filled"
                    margin="normal"
                    required
                    fullWidth
                    value={values.coupon}
                    onChange={handleInputChange}
                    name="coupon"
                    label="Cupom ID"
                    id="couponId"
                    type="text"
                    disabled={loading}
                    className={classes.textField}
                    InputProps={{
                        className: classes.input,
                    }}
                />
                <Typography
                    style={{ color: color }}
                    variant="caption"
                    display="block"
                    align="center"
                >
                    {message}
                </Typography>

                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    className={classes.submit}
                    disabled={loading}
                >
                    Criar usuário
                </Button>
            </form>
            {loading && <LinearProgress />}
        </FormControl>
    );
};

export default LoginFormFields;
