import React from "react";
import CmtCardHeader from "../../@coremat/CmtCard/CmtCardHeader";
import CmtAdvCardContent from "../../@coremat/CmtAdvCard/CmtAdvCardContent";
import CmtAdvCard from "../../@coremat/CmtAdvCard";
import makeStyles from "@material-ui/core/styles/makeStyles";
import StatisticList from "./StatisticList";
import Box from "@material-ui/core/Box";
import SalesGauge from "./SalesGauge";
import SalesAreaChart from "./SalesAreaChart";

import GridContainer from "./GridContainer";
import Grid from "@material-ui/core/Grid";

import { useGlobalData } from "../hooks/DataContext";

import { getYearValueChart, getActiveSubscribers } from "../../utils/paymentFunctions";

import { EventAvailable, DateRange, LocalAtm, EventNote, RotateLeft } from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
    cardContentRoot: {
        position: "relative",
    },
    subTitleRoot: {
        fontSize: 12,
        color: theme.palette.text.secondary,
    },
    salesGaugeRoot: {
        color: theme.palette.text.primary,
        "& text": {
            fill: theme.palette.text.primary,
        },
    },
}));

/* 
    const lineChartData = [
        { name: "Jan", uv: 9000 },
        { name: "Feb", uv: 12000 },
        { name: "Mar", uv: 7000 },
        { name: "Apr", uv: 12000 },
        { name: "May", uv: 14000 },
        { name: "Jun", uv: 18500 },
        { name: "Jul", uv: 14000 },
        { name: "Aug", uv: 17000 },
        { name: "Sep", uv: 15000 },
        { name: "Oct", uv: 18000 },
        { name: "Nov", uv: 19500 },
        { name: "Dec", uv: 16000 },
    ]; */

const getGaugeChartData = (number) => {
    let firstLevel,
        secondLevel,
        thirdLevel = 0;

    if (number < 10) {
        firstLevel = number * 10;
    } else if (number < 25) {
        firstLevel = 100;
        secondLevel = ((number - 10) / 15) * 100;
    } else if (number < 50) {
        firstLevel = 100;
        secondLevel = 100;
        thirdLevel = ((number - 25) / 25) * 100;
    } else {
        firstLevel = 100;
        secondLevel = 100;
        thirdLevel = 100;
    }

    return [
        {
            category: "Nível 1 - 20%",
            value: firstLevel,
            full: 100,
        },
        {
            category: "Nível 2 - 30%",
            value: secondLevel,
            full: 100,
        },
        {
            category: "Nível 3 - 35%",
            value: thirdLevel,
            full: 100,
        },
    ];
};

const SalesStatistic = ({ data }) => {
    const classes = useStyles();
    const { invoices, customers } = useGlobalData();

    const { chartData: linearChartData, total } = getYearValueChart(invoices);
    const activeSubscribers = getActiveSubscribers(customers);

    const salesGaugeChartData = getGaugeChartData(activeSubscribers);

    return (
        <CmtAdvCard>
            <CmtCardHeader title={"Histórico Anual"} />
            <CmtAdvCardContent className={classes.cardContentRoot}>
                <Box mb={{ xs: 5, sm: 8, lg: 11 }}>
                    <StatisticList statisticList={data} />
                </Box>
                <GridContainer>
                    <Grid item xs={12} lg={8}>
                        <SalesAreaChart salesStatisticData={linearChartData} />
                    </Grid>
                    <Grid item xs={12} lg={4}>
                        <Box className={classes.salesGaugeRoot}>
                            <SalesGauge salesGaugeData={salesGaugeChartData} />
                        </Box>
                    </Grid>
                </GridContainer>
            </CmtAdvCardContent>
        </CmtAdvCard>
    );
};

export default SalesStatistic;
