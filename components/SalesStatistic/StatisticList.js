import React from "react";
import StatisticItem from "./StatisticItem";
import GridContainer from "./GridContainer";

const StatisticList = ({ statisticList }) => {
    if (!statisticList) return null;
    return (
        <GridContainer justify="center">
            {statisticList.map((item, index) => (
                <StatisticItem item={item} key={index} />
            ))}
        </GridContainer>
    );
};

export default StatisticList;
