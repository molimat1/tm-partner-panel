import React, { useState, useEffect } from "react";

import axios from "axios";
import moment from "moment";
import {
  FormControl,
  TextField,
  Button,
  Typography,
  Grid,
  Link,
  CircularProgress,
  Box,
  Select,
  MenuItem,
} from "@material-ui/core";

import { makeStyles, withStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  form: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  submit: {
    margin: theme.spacing(2, 0, 2),
    width: "100px",
    backgroundColor: "#0c9",
    color: "#fcfcfc",
    "&:hover": {
      color: "#222",
    },
    transition: "all .4s",
  },
  select: {
    "& ul": {
      backgroundColor: "#FFF",
    },
    "& li": {
      fontSize: 14,
    },
  },
  textField: {
    width: "300px",
    paddingBottom: 0,

    fontWeight: 500,
  },
  input: {
    backgroundColor: "#ececec",
    height: 50,
    marginRight: 20,
    color: "#000",
    "&:hover": {
      color: "#fff",
    },
  },
  inputBox: {
    width: "100%",
    height: "20%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    width: "100%",
    height: "450px",
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
  },
}));

const initialFValues = {
  name: "",
};

const ValidationTextField = withStyles({
  root: {
    "& .MuiFormLabel-root.Mui-focused": {
      color: "#0c9",
    },

    "& .MuiFilledInput-underline::after": {
      borderBottom: "2px solid #0c9",
    },
    "& .Mui-focused": {
      color: "#fff",
    },
  },
})(TextField);

const LoginFormFields = () => {
  const classes = useStyles();

  const [email, setEmail] = useState("");
  const [currency, setCurrency] = useState("brl");
  const [days, setDays] = useState();
  const [message, setMessage] = useState("");
  const [info, setInfo] = useState("");
  const [loading, setLoading] = useState(false);
  const [price, setPrice] = useState("esports");

  const resetForm = () => {
    setEmail("");
    setDays();
  };

  const createTrial = async (email, days = 1) => {
    setLoading(true);

    let price_id = "";

    switch (price) {
      case "casino":
        if (currency === "usd") {
          price_id = "price_1OMb19GNlxYDgOEDxo3ZNgcX";
        }
        if (currency === "brl") {
          price_id = "price_1O8KIKGNlxYDgOEDbFltpFWJ";
        }
        break;
      case "soccer":
        if (currency === "usd") {
          price_id = "price_1OMZcSGNlxYDgOEDun9WFiip";
        }
        if (currency === "brl") {
          price_id = "price_1PYsacGNlxYDgOEDPt8amlHn";
        }
        break;
      case "tipster":
        if (currency === "usd") {
          price_id = "price_1KvlfCGNlxYDgOEDRHGemrpC";
        }
        if (currency === "brl") {
          price_id = "price_1N3l8ZGNlxYDgOEDnJUhdnHh";
        }
        break;
      case "esports":
        if (currency === "usd") {
          price_id = "price_1K0qzuGNlxYDgOEDjCKfP4mH";
        }
        if (currency === "brl") {
          price_id = "price_1NC95AGNlxYDgOEDIVqENvtT";
        }
        break;
      default:
        break;
    }

    const trialUser = {
      days,
      email: email.replaceAll(" ", "").toLowerCase(),
      currency,
      price: price_id,
    };

    if (email) {
      try {
        const { data } = await axios.post("/api/createTrialStripe", trialUser);
        setInfo(data?.info);
        setMessage("");
      } catch (e) {
        setInfo("");

        setMessage(e.response.data);
      }
    } else {
      setMessage("Preencha os campos!");
    }
    setLoading(false);
    resetForm();
  };

  return (
    <div className={classes.container}>
      <div className={classes.form}>
        <ValidationTextField
          variant="filled"
          margin="normal"
          required
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          autoFocus
          id="email"
          label="E-mail"
          fullWidth
          name="email"
          disabled={loading}
          className={classes.textField}
          InputProps={{
            className: classes.input,
          }}
        />
        <Select
          MenuProps={{ classes: { paper: classes.select } }}
          style={{ backgroundColor: "#eee", height: 45, marginTop: 10 }}
          value={price}
          onChange={(e) => setPrice(e.target.value)}
          fullWidth
        >
          <MenuItem value="casino">Plano Casino</MenuItem>
          <MenuItem value="esports">Plano Bot</MenuItem>
          <MenuItem value="soccer">Plano Soccer Bot</MenuItem>
          <MenuItem value="tipster">Plano Tipster</MenuItem>
        </Select>
        <Box display="flex" style={{ gap: 10 }} alignItems="center">
          <Select
            MenuProps={{ classes: { paper: classes.select } }}
            style={{ backgroundColor: "#eee", height: 45, marginTop: 10 }}
            value={currency}
            onChange={(e) => setCurrency(e.target.value)}
          >
            <MenuItem value="usd">USD</MenuItem>
            <MenuItem value="brl">BRL</MenuItem>
          </Select>

          <ValidationTextField
            variant="filled"
            margin="normal"
            required
            value={days}
            onChange={(e) => setDays(e.target.value)}
            autoFocus
            id="days"
            label="Dias de Trial"
            name="days"
            disabled={loading}
            style={{
              width: "150px",
              paddingBottom: 0,

              fontWeight: 500,
            }}
            InputProps={{
              className: classes.input,
            }}
          />
        </Box>

        <Button
          type="submit"
          fullWidth
          variant="contained"
          className={classes.submit}
          disabled={loading}
          onClick={() => createTrial(email, days)}
        >
          Criar
        </Button>
      </div>
      <div
        style={{
          width: "100%",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {loading ? (
          <div>
            <CircularProgress style={{ color: "#0c9", marginLeft: 20 }} />{" "}
            <Typography align="center" style={{ color: "white" }}>
              Carregando
            </Typography>
          </div>
        ) : (
          <Box>
            {message ? (
              <Typography align="center" style={{ color: "white" }}>
                {message}
              </Typography>
            ) : (
              <Box>
                {info && (
                  <Typography align="center" style={{ color: "#0c9" }}>
                    {info}
                  </Typography>
                )}
              </Box>
            )}
          </Box>
        )}
      </div>
    </div>
  );
};

export default LoginFormFields;
