import React, { useState } from "react";

import {
  Button,
  Typography,
  CircularProgress,
  Select,
  MenuItem,
  TextField,
} from "@material-ui/core";

import { makeStyles, withStyles } from "@material-ui/core/styles";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  form: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  submit: {
    margin: theme.spacing(2, 0, 2),
    width: "100px",
    backgroundColor: "#0c9",
    color: "#fcfcfc",
    "&:hover": {
      color: "#222",
    },
    transition: "all .4s",
  },
  textField: {
    width: "300px",
    paddingBottom: 0,
    backgroundColor: "#ececec",
    fontWeight: 500,
  },
  input: {
    backgroundColor: "#ececec",
    height: 50,

    color: "#000",

    "& .MuiFormLabel-root.Mui-focused": {
      color: "#0c9",
    },
    width: 300,
    "& .MuiFilledInput-underline::after": {
      borderBottom: "2px solid #0c9",
    },
    "& .Mui-focused": {
      color: "#fff",
    },
  },
  inputBox: {
    width: "100%",
    height: "20%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    width: "100%",
    height: "450px",
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
  },
}));

const ValidationTextField = withStyles({
  root: {
    "& .MuiFormLabel-root.Mui-focused": {
      color: "#0c9",
    },

    "& .MuiFilledInput-underline::after": {
      borderBottom: "2px solid #0c9",
    },
  },
})(TextField);

const RedefinePass = () => {
  const classes = useStyles();

  const [message, setMessage] = useState("");
  const [loading, setLoading] = useState(false);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);

  const getPayementsAPI = async () => {
    setLoading(true);
    setError(false);
    if (!email || password?.length < 7) {
      setError(true);
      setMessage("Garanta que a password tenha 8 caracteres");
      setLoading(false);
      return null;
    }

    try {
      const { data } = await axios.post(
        "/api/redefine-password",
        { email: email, password: password },
        {
          headers: { "Content-Type": "application/json" },
        }
      );
      console.log(data);
      setMessage("OK!");
      setEmail("");
    } catch (e) {
      console.log(e.response.data);
      setMessage("ERRO: " + e.response.data.error);
    }

    setLoading(false);
  };

  return (
    <div className={classes.container}>
      <ValidationTextField
        variant="filled"
        margin="normal"
        required
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        autoFocus
        id="email"
        label="E-mail"
        name="email"
        error={error}
        disabled={loading}
        className={classes.textField}
        InputProps={{
          className: classes.input,
        }}
      />
      <ValidationTextField
        variant="filled"
        margin="normal"
        required
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        autoFocus
        id="password"
        label="Senha"
        name="password"
        error={error}
        disabled={loading}
        className={classes.textField}
        InputProps={{
          className: classes.input,
        }}
      />
      <div className={classes.form}>
        <Button
          type="submit"
          fullWidth
          variant="contained"
          className={classes.submit}
          disabled={loading}
          onClick={() => getPayementsAPI()}
        >
          Enviar
        </Button>
      </div>
      <div
        style={{
          width: "100%",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {loading ? (
          <div>
            <CircularProgress style={{ color: "#0c9", marginLeft: 20 }} />{" "}
            <Typography align="center" style={{ color: "white" }}>
              Carregando
            </Typography>
          </div>
        ) : (
          <div>
            <Typography align="center" style={{ color: "white" }}>
              {message}
            </Typography>
          </div>
        )}
      </div>
    </div>
  );
};

export default RedefinePass;
