import React from "react";
import SignupGraph from "./SignupGraph";
import StatisticsClassicCard from "./StatisticsClassicCard";
import { useTheme } from "@material-ui/core/styles";

const OnlineSignups = ({
  data,
  title,
  subtitle,
  color,
  growth,
  xAxis,
  tooltip,
}) => {
  console.log(
    JSON.stringify({ data, title, subtitle, color, growth, xAxis, tooltip })
  );

  return (
    <StatisticsClassicCard
      backgroundColor={["#E2EEFF -18.96%", "#fff 108.17%"]}
      gradientDirection="180deg"
      color={color}
      title={title}
      subTitle={subtitle}
      growth={growth}
    >
      <SignupGraph data={data} color={color} xAxis={xAxis} tooltip={tooltip} />
    </StatisticsClassicCard>
  );
};

export default OnlineSignups;
