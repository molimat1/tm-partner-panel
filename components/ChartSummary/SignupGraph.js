import React from "react";
import { Line, LineChart, ResponsiveContainer, Tooltip, XAxis } from "recharts";
import Box from "@material-ui/core/Box";

const SignupGraph = ({ data, color, xAxis, tooltip }) => {
    return (
        <ResponsiveContainer width="100%" height={120}>
            <LineChart data={data} margin={{ top: 10, right: 0, left: 0, bottom: 3 }}>
                <Tooltip
                    cursor={false}
                    content={({ active, label, payload }) => {
                        return active ? (
                            <Box color="#fff">
                                {payload.map((row, index) => (
                                    <Box key={index}>{`${label}: ${tooltip} ${row.value}`}</Box>
                                ))}
                            </Box>
                        ) : null;
                    }}
                    wrapperStyle={{
                        background: color,
                        padding: "5px 8px",
                        borderRadius: 4,
                        overflow: "hidden",
                    }}
                />
                <XAxis dataKey={xAxis} hide />
                <Line dataKey="count" type="monotone" dot={null} strokeWidth={3} stackId="2" stroke={color} />
            </LineChart>
        </ResponsiveContainer>
    );
};

export default SignupGraph;
