import Button from "@material-ui/core/Button";
import Fade from "@material-ui/core/Fade";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import PopupState, { bindPopper, bindToggle } from "material-ui-popup-state";
import React from "react";
import { getFormattedDate } from "../../utils/dateHelper";
import { useGlobalData } from "../hooks/DataContext";

const useStyles = makeStyles((theme) => ({
    typography: {
        padding: theme.spacing(2),
    },
    couponItem: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        width: "100%",
    },
    title: {
        fontWeight: 500,
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        width: "100%",
    },
    titleText: {
        fontWeight: 600,
        fontSize: 10,
    },
    couponText: {
        fontSize: 10,
    },
}));

const CouponsButton = withStyles({
    root: {
        backgroundColor: "#0c9",
        color: "#fff",
        "&:hover": {
            background: "#007852",
        },
        borderRadius: 5,
        border: 0,
        height: 28,
        fontSize: 12,
        fontWeight: 600,
        padding: "2px 7px 0px",
        marginLeft: 20,
        transition: "all .4s ease",
        WebkitTransition: "all .4s ease",
        MozTransition: "all .4s ease",
    },
    label: {
        textTransform: "Capitalize",
    },
})(Button);

export default function PopperPopupState() {
    const classes = useStyles();
    const { coupons } = useGlobalData();

    return (
        <PopupState variant="popper" popupId="demo-popup-popper">
            {(popupState) => (
                <div>
                    <CouponsButton {...bindToggle(popupState)}>Meus Cupons</CouponsButton>
                    <Popper {...bindPopper(popupState)} transition style={{ zIndex: 5000, marginTop: 20 }} placement="bottom-end">
                        {({ TransitionProps }) => (
                            <Fade {...TransitionProps} timeout={350}>
                                <Paper style={{ padding: 10 }}>
                                    <div className={classes.title}>
                                        <Typography className={classes.titleText} style={{ width: 50 }}>
                                            Código
                                        </Typography>
                                        <Typography className={classes.titleText} style={{ width: 70 }}>
                                            Criado em
                                        </Typography>
                                        <Typography className={classes.titleText} style={{ width: 70 }}>
                                            Expira em
                                        </Typography>
                                        <Typography className={classes.titleText} style={{ width: 75 }}>
                                            Vezes usado
                                        </Typography>
                                        <Typography className={classes.titleText} style={{ width: 50 }}>
                                            Status
                                        </Typography>
                                    </div>
                                    {coupons?.map((coupon, key) => {
                                        return (
                                            <div key={key} className={classes.couponItem}>
                                                <Typography className={classes.couponText} style={{ width: 50 }}>
                                                    {coupon.code}
                                                </Typography>
                                                <Typography className={classes.couponText} style={{ width: 70 }}>
                                                    {getFormattedDate(coupon.created)}
                                                </Typography>
                                                <Typography className={classes.couponText} style={{ width: 70 }}>
                                                    {getFormattedDate(coupon.expires_at) || "-"}
                                                </Typography>
                                                <Typography className={classes.couponText} style={{ width: 75 }} align="center">
                                                    {coupon.times_redeemed}
                                                </Typography>
                                                <Typography
                                                    className={classes.couponText}
                                                    style={{
                                                        width: 50,
                                                        color: coupon.active ? "#0c9" : "#999",
                                                    }}
                                                    align="left">
                                                    {coupon.active ? "Ativo" : "Inativo"}
                                                </Typography>
                                            </div>
                                        );
                                    })}
                                </Paper>
                            </Fade>
                        )}
                    </Popper>
                </div>
            )}
        </PopupState>
    );
}
