import React, { useState } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import InputBase from "@material-ui/core/InputBase";
import {
  alpha,
  makeStyles,
  useTheme,
  withStyles,
} from "@material-ui/core/styles";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import SearchIcon from "@material-ui/icons/Search";

import Coupons from "./CouponsPopper";

import { useRouter } from "next/router";

import { useGlobalData } from "../hooks/DataContext";
import { Box, Drawer, useMediaQuery } from "@material-ui/core";
import Menu from "@material-ui/icons/Menu";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  userInfo: {
    paddingLeft: 20,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    textTransform: "uppercase",
    marginRight: 40,
    color: "white",
    "&:hover": {
      color: "#0c9",
    },
  },
  titleActive: {
    textTransform: "uppercase",
    marginRight: 40,
    color: "#0c9",
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,

    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "20ch",
      "&:focus": {
        width: "30ch",
      },
    },
  },
  links: {
    display: "flex",
    justifyContent: "flex-start",
    flex: 1,
  },
  toolbar: {
    display: "flex",
    justifyContent: "space-between",
  },
}));

export default function SearchAppBar({ user, signOut }) {
  const { resetData } = useGlobalData();
  const theme = useTheme();
  const mdUp = useMediaQuery(theme.breakpoints.up("md"));
  const router = useRouter();
  const classes = useStyles();
  const path = router.pathname;

  const [openDrawer, setOpenDrawer] = useState(false);

  const toggleDrawer = (open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setOpenDrawer(open);
  };

  const logout = () => {
    resetData();
    setOpenDrawer(false);
    signOut();
  };

  return (
    <div className={classes.root}>
      {mdUp ? (
        <AppBar position="fixed" style={{ backgroundColor: "#222" }}>
          <Toolbar className={classes.toolbar}>
            {/* <IconButton
                        edge="start"
                        className={classes.menuButton}
                        color="inherit"
                        aria-label="open drawer"
                    >
                        <MenuIcon />
                    </IconButton> */}
            <div className={classes.links}>
              <Button
                className={
                  path === "/dashboard" ? classes.titleActive : classes.title
                }
                onClick={() => router.push("/dashboard")}
              >
                Dashboard
              </Button>
              <Button
                className={
                  path === "/payments" ? classes.titleActive : classes.title
                }
                onClick={() => router.push("/payments")}
              >
                Pagamentos
              </Button>
              <Button
                className={
                  path === "/clients" ? classes.titleActive : classes.title
                }
                onClick={() => router.push("/clients")}
              >
                Clientes
              </Button>
              {user.admin && (
                <Button
                  className={
                    path === "/admin" ? classes.titleActive : classes.title
                  }
                  onClick={() => router.push("/admin")}
                >
                  Admin
                </Button>
              )}
              {user.admin && (
                <Button
                  className={
                    path === "/email" ? classes.titleActive : classes.title
                  }
                  onClick={() => router.push("/email")}
                >
                  Email
                </Button>
              )}
              {user.admin && (
                <Button
                  className={
                    path === "/suggestions"
                      ? classes.titleActive
                      : classes.title
                  }
                  onClick={() => router.push("/suggestions")}
                >
                  Sugestões
                </Button>
              )}
            </div>
            {/* <div className={classes.search}>
                        <div className={classes.searchIcon}>
                            <SearchIcon />
                        </div>
                        <InputBase
                            placeholder="Search…"
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{ "aria-label": "search" }}
                        />
                    </div> */}
            <div className={classes.userInfo}>
              <Typography
                style={{
                  fontSize: 14,
                  padding: 0,
                  lineHeight: 1.8,
                }}
              >
                <strong>{user.full_name}</strong>
              </Typography>
              <div style={{ display: "flex" }}>
                <Typography
                  style={{
                    fontSize: 9,
                    marginRight: 5,
                    lineHeight: 1,
                  }}
                >
                  <strong>E-mail: </strong> {user.email}
                </Typography>
              </div>
            </div>
            <Coupons />
            <IconButton
              className={classes.button}
              aria-label="Delete"
              onClick={logout}
            >
              <ExitToAppIcon style={{ color: "white" }} />
            </IconButton>
          </Toolbar>
        </AppBar>
      ) : (
        <AppBar position="fixed" style={{ backgroundColor: "#222" }}>
          <Toolbar className={classes.toolbar}>
            <Box display="flex" justifyContent={"space-between"} width="100%">
              <div className={classes.userInfo}>
                <Typography
                  style={{
                    fontSize: 14,
                    padding: 0,
                    lineHeight: 1.8,
                  }}
                >
                  <strong>{user.full_name}</strong>
                </Typography>
                <div style={{ display: "flex" }}>
                  <Typography
                    style={{
                      fontSize: 9,
                      marginRight: 5,
                      lineHeight: 1,
                    }}
                  >
                    <strong>E-mail: </strong> {user.email}
                  </Typography>
                </div>
              </div>
              <IconButton onClick={toggleDrawer(true)}>
                <Menu style={{ color: "white" }} />
              </IconButton>
            </Box>
            <Drawer
              anchor={"right"}
              open={openDrawer}
              onClose={toggleDrawer(false)}
              PaperProps={{
                style: {
                  width: "100%",
                  maxWidth: 300,
                  backgroundColor: "#222",
                  paddingTop: "2vh",
                },
              }}
            >
              <Box display="flex" gap={2} flexDirection="column">
                <Button
                  className={
                    path === "/dashboard" ? classes.titleActive : classes.title
                  }
                  onClick={() => {
                    router.push("/dashboard");
                    setOpenDrawer(false);
                  }}
                >
                  Dashboard
                </Button>
                <Button
                  className={
                    path === "/payments" ? classes.titleActive : classes.title
                  }
                  onClick={() => {
                    router.push("/payments");
                    setOpenDrawer(false);
                  }}
                >
                  Pagamentos
                </Button>
                <Button
                  className={
                    path === "/clients" ? classes.titleActive : classes.title
                  }
                  onClick={() => {
                    router.push("/clients");
                    setOpenDrawer(false);
                  }}
                >
                  Clientes
                </Button>
                {user.admin && (
                  <Button
                    className={
                      path === "/admin" ? classes.titleActive : classes.title
                    }
                    onClick={() => {
                      router.push("/admin");
                      setOpenDrawer(false);
                    }}
                  >
                    Admin
                  </Button>
                )}
                {user.admin && (
                  <Button
                    className={
                      path === "/suggestions"
                        ? classes.titleActive
                        : classes.title
                    }
                    onClick={() => {
                      router.push("/suggestions");
                      setOpenDrawer(false);
                    }}
                  >
                    Sugestões
                  </Button>
                )}
                {user.admin && (
                  <Button
                    className={classes.title}
                    onClick={logout}
                    startIcon={<ExitToAppIcon style={{ color: "white" }} />}
                  >
                    Sair
                  </Button>
                )}
              </Box>
            </Drawer>
          </Toolbar>
        </AppBar>
      )}
    </div>
  );
}
