import React, { useEffect, useState } from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";
import moment from "moment";
import { format, parse, subDays, eachDayOfInterval } from "date-fns";
import StatisticsClassicCard from "../ChartSummary/StatisticsClassicCard";

function formatDate(dateString) {
  const date = parse(dateString, "yyyyMMdd", new Date());
  return format(date, "dd/MM/yy");
}

const fillEmptyDays = (emails) => {
  const startDate = subDays(new Date(), 30);
  const endDate =
    emails.length > 0
      ? parse(emails[emails.length - 1].date, "yyyyMMdd", new Date())
      : new Date();

  const dateRange = eachDayOfInterval({ start: startDate, end: endDate });

  const existingData = emails.reduce((acc, email) => {
    const date = parse(email.date, "yyyyMMdd", new Date());
    const formattedDate = format(date, "yyyyMMdd");
    acc[formattedDate] = {
      day: format(date, "dd/MM/yy"),
      OK: email.ok_qty,
      failed: email.error_qty,
    };
    return acc;
  }, {});

  const newData = dateRange.map((date) => {
    const formattedDate = format(date, "yyyyMMdd");
    return (
      existingData[formattedDate] || {
        day: format(date, "dd/MM/yy"),
        OK: 0,
        failed: 0,
      }
    );
  });

  return newData;
};

const EmailGraph = ({ emails }) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    if (!emails?.length) return;
    const newData = fillEmptyDays(emails);

    setData(newData);
  }, [emails]);

  return (
    <StatisticsClassicCard
      backgroundColor={["#E2EEFF -18.96%", "#fff 108.17%"]}
      gradientDirection="180deg"
      color={"#0c9"}
      title={""}
      subTitle={""}
      growth={null}
    >
      <ResponsiveContainer width="100%" height={240}>
        <LineChart
          data={data}
          margin={{ top: 10, right: 0, left: 0, bottom: 3 }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="day" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Line
            type="monotone"
            dataKey="OK"
            stroke="#34a853"
            name="Status OK"
          />
          <Line
            type="monotone"
            dataKey="failed"
            stroke="#ea4335"
            name="Status Falha"
          />
        </LineChart>
      </ResponsiveContainer>
    </StatisticsClassicCard>
  );
};

export default EmailGraph;
