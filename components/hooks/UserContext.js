import { useEffect, useState, createContext, useContext } from "react";
import { supabase } from "../../utils/initSupabase";

export const UserContext = createContext();

export const UserContextProvider = (props) => {
    const [userLoaded, setUserLoaded] = useState(false);
    const [loading, setLoading] = useState(true);
    const [session, setSession] = useState(null);
    const [user, setUser] = useState(null);
    const [userDetails, setUserDetails] = useState(null);
    const [subscription, setSubscription] = useState(null);

    useEffect(() => {
        setLoading(true);
        const session = supabase.auth.session();
        setSession(session);
        setUser(session?.user ?? null);
        const { data: authListener } = supabase.auth.onAuthStateChange(async (event, session) => {
            setSession(session);
            setUser(session?.user ?? null);
        });
        setLoading(false);
        return () => {
            authListener.unsubscribe();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    // Get the user details.
    const getUserDetails = async () => await supabase.from("users").select("*").eq("id", user.id);

    const updateName = (name, userId) =>
        supabase
            .from("users")
            .update({
                full_name: name,
            })
            .eq("id", userId);

    useEffect(() => {
        setLoading(true);
        if (user) {
            getUserDetails().then((results) => {
                setUserDetails(results.data);
                setUserLoaded(true);
                if (results.status === 401) value.signOut();
            });
        }
        setLoading(false);
    }, [user]);

    const value = {
        session,
        user,
        userDetails,
        userLoaded,
        loading,
        subscription,
        signIn: (options) => supabase.auth.signIn(options),
        signUp: (options) => supabase.auth.signUp(options),
        updateName: (name, userId) => updateName(name, userId),
        signOut: () => {
            setUserDetails(null);
            setSubscription(null);
            setUserLoaded(false);
            return supabase.auth.signOut();
        },
        passReset: (email) => supabase.auth.api.resetPasswordForEmail(email),
        setNewPass: (accessToken, newPass) =>
            supabase.auth.api.updateUser(accessToken, {
                password: newPass,
            }),
    };
    return <UserContext.Provider value={value} {...props} />;
};

export const useUser = () => {
    const context = useContext(UserContext);
    if (context === undefined) {
        throw new Error(`useUser must be used within a UserContextProvider.`);
    }
    return context;
};
