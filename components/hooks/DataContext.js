import React, { useContext, useState, createContext } from "react";

const DataContext = createContext({});

export const useGlobalData = () => useContext(DataContext);

export function DataProvider({ children }) {
    const [invoices, setInvoices] = useState([]);
    const [coupons, setCoupons] = useState([]);
    const [products, setProducts] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [populated, setPopulated] = useState(false);

    const resetData = () => {
        setPopulated(false);
        setCoupons([]);
        setInvoices([]);
        setProducts([]);
        setCustomers([]);
    };

    return (
        <DataContext.Provider
            value={{
                invoices,
                setInvoices,
                customers,
                setCustomers,
                coupons,
                setCoupons,
                products,
                setProducts,
                populated,
                setPopulated,
                resetData,
            }}
        >
            {children}
        </DataContext.Provider>
    );
}
