import React, { useState, useEffect, useContext, createContext } from "react";

/* 
import firebaseClient from "./firebaseClientbckp";
import "firebase/auth"; */

const AuthFlowContext = createContext({});

export function AccessProvider({ children }) {
    const [login, setLogin] = useState(false);
    const [signUp, setSignUp] = useState(false);
    const [resetPass, setResetPass] = useState(false);

    function toggleLogin() {
        setLogin((login) => !login);
        //console.log(login);
    }
    function toggleSignUp() {
        setSignUp((signUp) => !signUp);
    }

    function toggleResetPass() {
        setResetPass((resetPass) => !resetPass);
    }

    return (
        <AuthFlowContext.Provider
            value={{
                login,
                toggleLogin,
                resetPass,
                toggleResetPass,
                toggleSignUp,
                signUp,
            }}
        >
            {children}
        </AuthFlowContext.Provider>
    );
}
/* 
export const AuthProvider = ({ children }) => {
    firebaseClient();
    const [user, setUser] = useState(null);
    const [loading, setLoading] = useState(false);
    const [premium, setPremium] = useState(null);

    const router = useRouter();

    useEffect(() => {
        return firebase.auth().onIdTokenChanged(async (user) => {
            setLoading(true);
            if (!user) {
                setUser(null);
                nookies.set(undefined, "token", "", {});

                setLoading(false);
                return;
            }
            const token = await user.getIdToken();
            setUser(user);
            const userFromDB = await database(user.uid, null, "GET_USER");
            nookies.set(undefined, "token", token, {
                maxAge: 24 * 60 * 60,
                sameSite: "lax",
                secure: "true",
            });
            setPremium(userFromDB.premium);
            setLoading(false);
        });
    }, []);

    return (
        <AuthContext.Provider value={{ user, loading, premium }}>
            {children}
        </AuthContext.Provider>
    );
};
 */
export const useAuthFlow = () => useContext(AuthFlowContext);
