import React, { useState } from "react";

import { Button, Typography, CircularProgress } from "@material-ui/core";

import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import MonthPicker from "./MonthPicker";

const useStyles = makeStyles((theme) => ({
  form: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  submit: {
    margin: theme.spacing(2, 0, 2),
    width: "100px",
    backgroundColor: "#0c9",
    color: "#fcfcfc",
    "&:hover": {
      color: "#222",
    },
    transition: "all .4s",
  },
  textField: {
    width: "300px",
    paddingBottom: 0,

    fontWeight: 500,
  },
  input: {
    backgroundColor: "#ececec",
    height: 50,
    marginRight: 20,
    color: "#000",
    "&:hover": {
      color: "#fff",
    },
  },
  inputBox: {
    width: "100%",
    height: "20%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    width: "100%",
    height: "450px",
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
  },
}));

const LoginFormFields = () => {
  const classes = useStyles();

  const [message, setMessage] = useState("");
  const [period, setPeriod] = useState({
    month: new Date().getMonth(),
    year: new Date().getFullYear(),
  });
  const [loading, setLoading] = useState(false);

  const getPayementsAPI = async () => {
    setLoading(true);

    window.open(`/api/getPaymentsAsCSV?month=${period.month}&year=${period.year}`, "_self");

    setLoading(false);
  };

  return (
    <div className={classes.container}>
      <MonthPicker onChange={(e) => setPeriod(e)} />
      <div className={classes.form}>
        <Button type="submit" fullWidth variant="contained" className={classes.submit} disabled={loading} onClick={() => getPayementsAPI()}>
          Exportar
        </Button>
      </div>
      <div
        style={{
          width: "100%",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {loading ? (
          <div>
            <CircularProgress style={{ color: "#0c9", marginLeft: 20 }} />{" "}
            <Typography align="center" style={{ color: "white" }}>
              Carregando
            </Typography>
          </div>
        ) : (
          <div>
            <Typography align="center" style={{ color: "white" }}>
              {message}
            </Typography>
          </div>
        )}
      </div>
    </div>
  );
};

export default LoginFormFields;
