import React, { useState, useEffect } from "react";

import axios from "axios";
import moment from "moment";
import { FormControl, TextField, Button, Typography, Grid, Link, CircularProgress, Box } from "@material-ui/core";

import { makeStyles, withStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  form: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  submit: {
    margin: theme.spacing(2, 0, 2),
    width: "100px",
    backgroundColor: "#0c9",
    color: "#fcfcfc",
    "&:hover": {
      color: "#222",
    },
    transition: "all .4s",
  },
  textField: {
    width: "300px",
    paddingBottom: 0,

    fontWeight: 500,
  },
  input: {
    backgroundColor: "#ececec",
    height: 50,
    marginRight: 20,
    color: "#000",
    "&:hover": {
      color: "#fff",
    },
  },
  inputBox: {
    width: "100%",
    height: "20%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    width: "100%",
    height: "450px",
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
  },
}));

const initialFValues = {
  name: "",
};

const ValidationTextField = withStyles({
  root: {
    "& .MuiFormLabel-root.Mui-focused": {
      color: "#0c9",
    },

    "& .MuiFilledInput-underline::after": {
      borderBottom: "2px solid #0c9",
    },
    "& .Mui-focused": {
      color: "#fff",
    },
  },
})(TextField);

const AddCredits = () => {
  const classes = useStyles();

  const [email, setEmail] = useState("");
  const [credits, setCredits] = useState("");
  const [message, setMessage] = useState("");
  const [info, setInfo] = useState("");
  const [loading, setLoading] = useState(false);

  const resetForm = () => {
    setEmail("");
    setCredits("");
  };

  console.log(credits);

  const createTrial = async (email, credits) => {
    setLoading(true);

    const userAndCreditInfo = { credits, email: email.replace(" ", "").toLowerCase() };

    console.log(credits);
    if (email) {
      try {
        const { data } = await axios.post("/api/addCredits", userAndCreditInfo);
        setInfo(data?.info);
        setMessage("");
      } catch (e) {
        console.log(e);
        setMessage("Erro adicionando créditos");
      }
    } else {
      setMessage("Preencha os campos!");
    }
    setLoading(false);
    resetForm();
  };

  return (
    <div className={classes.container}>
      <div className={classes.form}>
        <ValidationTextField
          variant="filled"
          margin="normal"
          required
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          autoFocus
          id="email"
          label="E-mail"
          name="email"
          disabled={loading}
          className={classes.textField}
          InputProps={{
            className: classes.input,
          }}
        />
        <ValidationTextField
          variant="filled"
          margin="normal"
          required
          value={credits}
          onChange={(e) => setCredits(e.target.value)}
          autoFocus
          id="credits"
          label="Qtd. de Créditos"
          name="credits"
          disabled={loading}
          className={classes.textField}
          InputProps={{
            className: classes.input,
          }}
        />

        <Button type="submit" fullWidth variant="contained" className={classes.submit} disabled={loading} onClick={() => createTrial(email, credits)}>
          Criar
        </Button>
      </div>
      <div
        style={{
          width: "100%",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {loading ? (
          <div>
            <CircularProgress style={{ color: "#0c9", marginLeft: 20 }} />{" "}
            <Typography align="center" style={{ color: "white" }}>
              Carregando
            </Typography>
          </div>
        ) : (
          <Box>
            {message ? (
              <Typography align="center" style={{ color: "white" }}>
                {message}
              </Typography>
            ) : (
              <Box>
                {info && (
                  <Typography align="center" style={{ color: "#0c9" }}>
                    {info}
                  </Typography>
                )}
              </Box>
            )}
          </Box>
        )}
      </div>
    </div>
  );
};

export default AddCredits;
