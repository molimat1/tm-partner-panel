import React from "react";

import {
    Container,
    Paper,
    Typography,
    Box,
    Link,
    Avatar,

    Modal,
    Backdrop,
    useMediaQuery,
} from "@material-ui/core";

import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import { makeStyles, alpha } from "@material-ui/core/styles";

import LoginFormFields from "./LoginFormFields";
import Logo from "./Logo";

import { useAuthFlow } from "./hooks/AuthFlow";

const useStyles = makeStyles((theme) => ({
    paper: {
        margin: theme.spacing(0),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        backgroundColor: alpha(theme.palette.common.white, 1),
    },
    avatar: {
        margin: theme.spacing(2),
        backgroundColor: theme.palette.success.light,
    },
    modal: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
}));

export default function LoginForm() {
    const classes = useStyles();
    const { toggleLogin } = useAuthFlow();
    const smUp = useMediaQuery('(min-width:600px)');

    return (
        <div
            style={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "flex-end",
                alignItems: "center",
            }}
        >
            <div style={{ margin: 50 }}>
                <Logo color="#fff" secondaryColor="#0c9" width={smUp ? "400px" : "60vw"}  />
            </div>

            <Typography
                component="h1"
                variant="h4"
                style={{ color: "#fcfcfc" }}
            >
                Portal de Parceiros
            </Typography>
            <LoginFormFields smUp={smUp} />
        </div>
    );
}
