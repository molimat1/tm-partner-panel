import React from "react";

import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";

import Logo from "./Logo";

import { makeStyles } from "@material-ui/core/styles";
import { Typography, useMediaQuery } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: "#fff",
    },
}));

const Loading = ({ message }) => {
    const classes = useStyles();
    const smUp = useMediaQuery('(min-width:600px)');

    return (
        <Backdrop className={classes.backdrop} open={true}>
            <div style={{ display: "flex", flexDirection: "column", alignItems:"center" }}>
                <Logo width={smUp ? "400px" : "60vw"} activate={true} color={"#06D6A0"} />
                <Typography
                    variant="subtitle1"
                    color="textSecondary"
                    align="center"
                    style={{ marginTop: 20, color: "#fafafa", fontWeight: 500 }}
                >
                    {message}
                </Typography>
            </div>
        </Backdrop>
    );
};

export default Loading;
