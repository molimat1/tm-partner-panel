import React, { useState, useEffect } from "react";

import axios from "axios";
import moment from "moment";
import {
  FormControl,
  TextField,
  Button,
  Typography,
  Grid,
  Link,
  CircularProgress,
  Paper,
  Box,
  useTheme,
  useMediaQuery,
  Divider,
} from "@material-ui/core";

import { makeStyles, styled, withStyles } from "@material-ui/core/styles";
import { supabase } from "../utils/initSupabase";
import { useRouter } from "next/router";
import { useUser } from "./hooks/UserContext";
import SpringModal from "./Modal";

import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import TipAppKeyComponent from "./TipAppKeyComponent";

const useStyles = makeStyles((theme) => ({
  form: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  submit: {
    margin: theme.spacing(2, 0, 2),
    width: "100px",
    backgroundColor: "#0c9",
    color: "#fcfcfc",
    "&:hover": {
      color: "#222",
    },
    transition: "all .4s",
  },
  textField: {
    width: "200px",
    [theme.breakpoints.up("md")]: {
      width: "300px",
    },
    paddingBottom: 0,

    fontWeight: 500,
  },
  input: {
    backgroundColor: "#ececec",
    height: 50,
    marginRight: 20,

    color: "#000",
    "&:hover": {
      color: "#fff",
    },
  },
  inputBox: {
    display: "flex",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
    },
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "100%",
    height: "450px",
  },
}));

const initialFValues = {
  name: "",
};

const ValidationTextField = withStyles({
  root: {
    "& .MuiFormLabel-root.Mui-focused": {
      color: "#0c9",
    },

    "& .MuiFilledInput-underline::after": {
      borderBottom: "2px solid #0c9",
    },
    "& .Mui-focused": {
      color: "#fff",
    },
  },
})(TextField);

const getSquare = (flag, char, small, color) => {
  if (!flag) return null;
  return (
    <Box
      height={15}
      borderRadius={5}
      p={small ? 0 : 1}
      width={small ? 15 : null}
      style={{ backgroundColor: flag ? color ?? "#30c" : "#333" }}
      display="flex"
      justifyContent={"center"}
      alignItems="center"
    >
      <Typography
        style={{ color: "white", fontSize: 9, paddingTop: small ? 3 : 2 }}
      >
        {char}
      </Typography>
    </Box>
  );
};
const SubscriptionCard = ({ sub, stripeId, guruId }) => {
  const theme = useTheme();
  const mdUp = useMediaQuery(theme.breakpoints.up("md"));

  console.log("SUB", sub);

  const checkOnProvider = async () => {
    if (sub.price_id) {
      window
        .open(`https://dashboard.stripe.com/customers/` + stripeId, "_blank")
        .focus();
    } else {
      window
        .open(`https://digitalmanager.guru/admin/contacts/` + guruId, "_blank")
        .focus();
    }
  };

  if (sub.status === "canceled") return null;
  return (
    <Paper
      elevation={4}
      style={{
        padding: 10,
        backgroundColor:
          sub.status === "past_due"
            ? "#f50"
            : sub.status === "trialing"
            ? "#29c"
            : sub.status === "incomplete_expired" ||
              sub.status === "canceled" ||
              sub.status === "unpaid" ||
              sub.status === "incomplete"
            ? "#999"
            : "#0c9",
        width: 300,
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
      }}
    >
      <Box display="flex" justifyContent={"space-between"}>
        <Box>
          <Typography style={{ fontSize: 12, fontWeight: 600 }}>
            {sub.name}
          </Typography>
          <Typography style={{ fontSize: 12, fontWeight: 300, color: "#333" }}>
            {sub.price_id ? "Stripe" : "Guru"}
          </Typography>
        </Box>
        <Typography style={{ fontSize: 12 }}>{sub.status}</Typography>
      </Box>

      <Box display="flex" justifyContent={"space-between"}>
        <Typography style={{ fontSize: 12 }}>
          <strong>Qtd:</strong> {sub.quantity}
        </Typography>
        {sub.status !== "past_due" && (
          <Typography style={{ fontSize: 12 }}>
            <strong>Vence em:</strong> {sub.current_period_end.slice(0, 10)}
          </Typography>
        )}
      </Box>
      {!!sub.cancel_at_period_end && (
        <Typography
          align="right"
          style={{ fontSize: 12, color: "#111", marginTop: 10 }}
        >
          Cancela no fim do período
        </Typography>
      )}
      <Box display="flex" justifyContent={"center"}>
        <Button
          variant="contained"
          style={{ marginTop: 10 }}
          size="small"
          onClick={() => checkOnProvider()}
        >
          <Typography style={{ fontSize: 11 }}>Ver no provedor</Typography>
        </Button>
      </Box>
    </Paper>
  );
};

const getTipsNumber = (i) => {
  let today = 0;
  let all = 0;
  if (!i?.length) return { today, all };

  i.sort((a, b) => (a.day > b.day ? -1 : 1));

  i.forEach((value) => {
    if ((value.day.slice(0, 10), new Date().toISOString().slice(0, 10))) {
      today = value.qty;
      all += value.qty;
    } else {
      all += value.qty;
    }
  });

  return { today, all, last_date: i[0].day.slice(0, 10) };
};

const RenderBot = ({ bot, casino }) => {
  const [openModal, setOpenModal] = useState(false);
  const theme = useTheme();
  const mdUp = useMediaQuery(theme.breakpoints.up("md"));

  const { today, all, last_date } = getTipsNumber(bot.agg_by_day);

  return (
    <Paper
      elevation={4}
      style={{
        padding: 5,
        backgroundColor: "#737372",
        width: 200,

        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
      }}
    >
      <Box display="flex" style={{ gap: 10 }}>
        <Box display="flex" justifyContent={"space-between"} width="100%">
          <Typography
            style={{
              fontSize: 12,
              fontWeight: 600,
              color: theme.palette.primary.main,
            }}
          >
            {bot.bot_name}
          </Typography>
          <Typography
            style={{
              fontSize: 10,
              color: theme.palette.secondary.main,
            }}
          >
            id: {bot.id_bot}
          </Typography>
        </Box>
      </Box>
      <Box display="flex" justifyContent={"space-between"}>
        <Typography style={{ fontSize: 10, fontWeight: 600, marginBottom: 5 }}>
          {getSport(bot?.cfg?.sport ?? bot?.cfg?.id_sport)}
        </Typography>
        <Typography style={{ fontSize: 10 }}>
          {markets[bot?.cfg?.market]}
        </Typography>
      </Box>
      {!!(
        bot.tipster ||
        bot.alert ||
        bot.disabled ||
        bot.marketplace ||
        bot.silenced ||
        bot.test
      ) && (
        <Box display="flex" alignItems="center" sx={{ gap: 1 }}>
          <Typography style={{ fontSize: 10 }}>flags:</Typography>
          {getSquare(bot.tipster, "T+", true)}
          {getSquare(bot.alert, "A", true)}
          {getSquare(bot.disabled, "D", true)}
          {getSquare(bot.marketplace, "M", true)}
          {getSquare(bot.marketplace_v2, "M2", true, "#0c9")}
          {getSquare(bot.silenced, "S", true)}
          {getSquare(bot.test, "T", true)}
        </Box>
      )}
      <Typography
        align="right"
        style={{ fontSize: 10, cursor: "pointer" }}
        onClick={() => setOpenModal(true)}
      >
        Ver todos dados
      </Typography>
      <SpringModal visible={openModal} onClose={() => setOpenModal(false)}>
        <Box p={2} display="flex" flexDirection={"column"} minWidth={"30vw"}>
          <Box
            display="flex"
            alignItems={"center"}
            justifyContent={mdUp ? "space-between" : "center"}
            flexDirection={mdUp ? "row" : "column"}
            sx={{ gap: mdUp ? 10 : 0 }}
            mb={0.5}
          >
            <Typography
              style={{
                fontSize: 12,
                fontWeight: 600,
                color: theme.palette.primary.main,
              }}
            >
              {bot.bot_name}
            </Typography>
            <Typography style={{ fontSize: 10 }}>
              {markets[bot?.cfg?.market]}
            </Typography>
            <Typography
              style={{
                fontSize: 10,
                color: theme.palette.secondary.main,
              }}
            >
              id: {bot.id_bot}
            </Typography>
          </Box>

          <Box
            display="flex"
            alignItems="center"
            justifyContent={"center"}
            sx={{ gap: 5 }}
          >
            {getSquare(bot.tipster, "tipster", false)}
            {getSquare(bot.alert, "alert", false)}
            {getSquare(bot.disabled, "disabled", false)}
            {getSquare(bot.marketplace, "marketplace", false)}
            {getSquare(bot.silenced, "silenced", false)}
            {getSquare(bot.test, "training", false)}
            {!!(!bot.silenced && !bot.test && !bot.disabled) &&
              getSquare(true, "active", false, "#0c9")}
          </Box>
          <Divider variant="middle" style={{ margin: 10 }} />
          <Box display="flex" justifyContent={"space-between"}>
            <Box>
              <Typography
                style={{ fontSize: 12, fontWeight: 600 }}
                align="center"
              >
                {today}
              </Typography>
              <Typography style={{ fontSize: 10 }} align="center">
                tips hoje
              </Typography>
            </Box>
            <Box>
              <Typography
                style={{ fontSize: 12, fontWeight: 600 }}
                align="center"
              >
                {last_date}
              </Typography>
              <Typography style={{ fontSize: 10 }} align="center">
                Última recebida
              </Typography>
            </Box>
            <Box>
              <Typography
                style={{ fontSize: 12, fontWeight: 600 }}
                align="center"
              >
                {all}
              </Typography>
              <Typography style={{ fontSize: 10 }} align="center">
                total
              </Typography>
            </Box>
          </Box>
          <Divider variant="middle" style={{ margin: 10 }} />
          <Box mt={1}>
            <Typography style={{ fontSize: 10, fontWeight: 600 }}>
              Config
            </Typography>
            <Box>
              {Object.entries(bot.cfg).map((i, k) => (
                <Box key={k}>
                  <Typography style={{ fontSize: 10, fontWeight: 600 }}>
                    {i[0]}: <span>{JSON.stringify(i[1])}</span>
                  </Typography>
                </Box>
              ))}
            </Box>
          </Box>
        </Box>
      </SpringModal>
    </Paper>
  );
};
const LoginFormFields = () => {
  const [email, setEmail] = useState("");
  const [lastSearch, setLastSearch] = useState("");
  const [info, setInfo] = useState({});
  const [message, setMessage] = useState("");
  const [mode, setMode] = useState("");
  const [openModal, setOpenModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const theme = useTheme();

  console.log(info);

  const handleInputChange = (e) => {
    const { value } = e.target;
    setEmail(value);
  };

  // const resetForm = () => {
  //   setEmail("");
  // };

  const handleChangeCrashVersion = async (v) => {
    setLoading(true);
    try {
      const { data } = await axios.get(
        "/api/change-crash-version?email=" + lastSearch + "&version=" + v
      );
      alert("Versão alterada");
    } catch (e) {
      alert("Algum erro ocorreu");
    }
    setLoading(false);
  };

  const createUser = async () => {
    setLoading("creating");
    try {
      const { data } = await axios.get("/api/createUser?email=" + lastSearch);

      setMode("");
      setMessage("Usuário criado");
    } catch (e) {
      console.log(e);
      alert("Algum erro ocorreu");
    }
    setLoading("");
  };

  const confirmUser = async () => {
    setLoading("confirming");
    try {
      const { data } = await axios.get(
        "/api/confirmationEmail?email=" + lastSearch
      );
      setMode("");
      setMessage(data.link);
    } catch (e) {
      console.log(e);
      alert("Algum erro ocorreu");
    }
    setLoading("");
  };

  const generateLink = async () => {
    try {
      const { data } = await axios.get(
        "/api/impersonateUser?email=" + lastSearch
      );

      console.log(data.link);
      if (data.link) window.open(data?.link, "_blank").focus();
    } catch (e) {
      console.log(e);
    }
  };

  const resetPassword = async () => {
    const answer = confirm("Certeza que quer resetar a senha usuário?");
    if (answer) {
      try {
        const { data } = await axios.get(
          "/api/reset-password?email=" + lastSearch
        );

        alert("Email enviado");
      } catch (e) {
        alert("Algum erro ocorreu");
      }
    }
  };

  const handleCreditsDeletion = async () => {
    try {
      const response = confirm(
        "Tem certeza que deseja deletar os créditos do usuário?"
      );
      if (response) {
        const { data } = await axios.get(
          "/api/deleteCredits?email=" + lastSearch
        );

        if (data.success) {
          setMessage("Deletando...");
          let { data } = await axios({
            method: "get",
            url: `/api/getUserData?email=${lastSearch}`,
          });

          if (data) {
            setLastSearch(lastSearch);
            setEmail("");
            setInfo(data);
          }
          setMessage("");
        }
      }
    } catch (e) {
      console.log(e);
    }
  };

  console.log("Emaik´", email);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    setMessage("");
    setMode("");
    setLastSearch(email.replaceAll(" ", "").toLowerCase());
    try {
      let { data } = await axios({
        method: "get",
        url: `/api/getUserData?email=${email
          .replaceAll(" ", "")
          .toLowerCase()}`,
      });

      if (data) {
        setInfo(data);
      }
    } catch (error) {
      if (error.response.status === 404) {
        setMessage("Usuário não encontrado");
        setMode("create");
      }
      if (error.response.status === 405) {
        setMessage("Usuário não confirmou o email");
        setMode("confirm");
      }
    }

    setLoading(false);
  };

  const classes = useStyles();

  return (
    <div className={classes.container}>
      <div className={classes.inputBox}>
        <ValidationTextField
          variant="filled"
          margin="normal"
          required
          value={email}
          onChange={handleInputChange}
          autoFocus
          id="email"
          label="Email"
          name="email"
          disabled={loading}
          className={classes.textField}
          InputProps={{
            className: classes.input,
          }}
        />

        <Button
          type="submit"
          fullWidth
          variant="contained"
          className={classes.submit}
          disabled={loading}
          onClick={handleSubmit}
        >
          Buscar
        </Button>
      </div>
      <Box
        display={"flex"}
        flexDirection={"column"}
        justifyContent={"center"}
        alignItems="center"
      >
        {!!message && (
          <Box
            style={{
              justifyContent: "center",
              alignItems: "center",
              display: "flex",
              flexDirection: "column",
              flexWrap: "wrap",
              marginTop: 10,
              marginBottom: 30,
            }}
          >
            <Box
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "column",

                flexWrap: "wrap",
              }}
            >
              <Typography style={{ color: "white", whiteSpace: "normal" }}>
                {message}
              </Typography>
              {!!message.includes("https") && (
                <Button
                  variant="contained"
                  onClick={() => navigator.clipboard.writeText(message)}
                >
                  Copiar link
                </Button>
              )}
            </Box>
            {mode === "create" && (
              <Button
                variant="contained"
                style={{ color: "green" }}
                onClick={createUser}
                disabled={loading === "creating"}
              >
                Convidar usuário
              </Button>
            )}
            {mode === "confirm" && (
              <Button
                variant="contained"
                style={{ color: "green" }}
                onClick={confirmUser}
              >
                Enviar email de confirmação
              </Button>
            )}
          </Box>
        )}
      </Box>
      <div
        style={{
          width: "100%",
          height: "80%",
          display: "flex",
          flexDirection: "column",
          overflow: "auto",
        }}
      >
        {loading ? (
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
              height: "100%",
              gap: 10,
            }}
          >
            <CircularProgress style={{ color: "#0c9" }} />{" "}
            <Typography align="center" style={{ color: "white" }}>
              Carregando
            </Typography>
          </div>
        ) : Object.keys(info).length > 0 ? (
          <div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                color: "white",
              }}
            >
              <Typography>
                <strong>Id: </strong>
                {info.id}
              </Typography>
              <Typography>
                <strong>Stripe: </strong>
                {info.stripeInfo?.id}
              </Typography>
              {info.guruId && (
                <Typography>
                  <strong>GURU ID: </strong>
                  {info.guruId}
                </Typography>
              )}
              <Typography>
                <strong>Balance: </strong>
                {info.stripeInfo?.balance}
              </Typography>
              <Typography>
                <strong>Créditos: </strong>
                {info.credits?.[0]?.current_amount}{" "}
                <span
                  style={{
                    textDecoration: "underline",
                    color: "red",
                    cursor: "pointer",
                  }}
                  onClick={() => handleCreditsDeletion()}
                >
                  zerar
                </span>
              </Typography>
              <Typography>
                <strong>Moeda: </strong>
                {info.stripeInfo?.currency}
              </Typography>
              <Typography>
                <strong>Cartão: </strong>
                {info.stripeInfo?.default_source ?? "Não tem"}
              </Typography>
            </div>
            <Box sx={{ display: "flex", justifyContent: "flex-start" }}>
              <TipAppKeyComponent
                code={info.bots?.[0]?.debug_slot}
                mutate={handleSubmit}
              />
            </Box>
            {console.log("INFO", info)}
            <Box my={1}>
              <Button
                variant="contained"
                style={{ fontSize: 12, color: "black" }}
                onClick={generateLink}
              >
                Acessar site como o usuário
              </Button>
            </Box>
            <Box my={1}>
              <Button
                variant="contained"
                style={{ fontSize: 12, color: "black", backgroundColor: "red" }}
                onClick={resetPassword}
              >
                Resetar senha
              </Button>
            </Box>
            <Box my={1} display="flex">
              <Button
                variant="contained"
                style={{
                  fontSize: 12,
                  color: "black",
                  backgroundColor: "gray",
                }}
                onClick={() => handleChangeCrashVersion("v1")}
                sx={{ mr: 1 }}
                disabled={loading}
              >
                v1 crash
              </Button>
              <Button
                variant="contained"
                style={{
                  fontSize: 12,
                  color: "black",
                  backgroundColor: "#0c9",
                  marginLeft: 10,
                }}
                onClick={() => handleChangeCrashVersion("v2")}
                disabled={loading}
              >
                v2 Crash
              </Button>
            </Box>
            <Grid container>
              <Grid item xs={12} lg={12} style={{ marginBottom: 10 }}>
                <Typography style={{ color: "white", fontWeight: 600 }}>
                  Assinaturas
                </Typography>

                <Box display="flex" sx={{ gap: 10 }} flexWrap="wrap">
                  {info?.subs?.length ? (
                    info?.subs.map((i, k) => {
                      return (
                        <SubscriptionCard
                          sub={i}
                          key={k}
                          stripeId={info.stripeInfo?.id}
                          guruId={info.guruId}
                        />
                      );
                    })
                  ) : (
                    <Typography style={{ color: "#ccc" }}>
                      Usuário não possui assinaturas
                    </Typography>
                  )}
                </Box>
              </Grid>

              <Grid item xs={12} lg={12}>
                <BotData
                  data={info?.bots}
                  setLastSearch={setLastSearch}
                  lastSearch={lastSearch}
                  setEmail={setEmail}
                  setInfo={setInfo}
                />
              </Grid>
            </Grid>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default LoginFormFields;

const StyledTableContainer = styled(TableContainer)(({ theme }) => ({
  marginBottom: theme.spacing(2),
  backgroundColor: "#aaa",
}));

const BotData = ({ data, lastSearch, setLastSearch, setEmail, setInfo }) => {
  const [loading, setLoading] = useState(false);
  return (
    <StyledTableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Bot Type</TableCell>
            <TableCell>Bots</TableCell>
            <TableCell>URL</TableCell>
            <TableCell>Paired</TableCell>
            <TableCell>Resetar</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((item, index) => (
            <TableRow key={index}>
              <TableCell>{item.bot_type}</TableCell>
              <TableCell>
                {item.bots.map((bot, index) => (
                  <div key={index}>
                    {Object.keys(bot)}: {Object.values(bot)}
                  </div>
                ))}
              </TableCell>
              <TableCell>{item.url}</TableCell>
              <TableCell>{item.paired}</TableCell>

              <TableCell>
                <Typography
                  onClick={async () => {
                    const confirmed = confirm(
                      "Resetar o telegram para o bot de sports"
                    );
                    if (confirmed) {
                      console.log("AAAH", confirmed);
                      setLoading(true);

                      console.log("lastSearch", lastSearch);
                      try {
                        let { data, status } = await axios({
                          method: "get",
                          url: `/api/reset-device?email=${lastSearch}&bot=${item.bot_type}`,
                        });
                        if (status === 200) {
                          console.log("Procurando novamente");

                          try {
                            let { data } = await axios({
                              method: "get",
                              url: `/api/getUserData?email=${lastSearch}`,
                            });

                            if (data) {
                              setLastSearch(lastSearch);
                              setEmail("");
                              setInfo(data);
                            }
                          } catch (error) {
                            console.log(error);
                            if (error.response.status === 404) {
                              setMessage("Usuário não encontrado");
                            }
                            if (error.response.status === 405) {
                              setMessage("Usuário não confirmou o email");
                            }
                          }
                        }
                      } catch (e) {
                        console.log(e);
                      }

                      setLoading(false);
                    }
                  }}
                  style={{
                    paddingLeft: 2,
                    fontSize: 12,
                    textDecoration: "underline",
                    color: "red",
                    cursor: "pointer",
                  }}
                >
                  resetar
                  {!!loading && (
                    <span style={{ paddingTop: 2 }}>
                      <CircularProgress size={14} style={{ color: "red" }} />
                    </span>
                  )}
                </Typography>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </StyledTableContainer>
  );
};

const getSport = (id) => {
  switch (id) {
    case 1:
      return "e-Soccer";
      break;
    case 2:
      return "e-Basket";
      break;
    case 3:
      return "e-Hockey";
      break;
    case 4:
      return "Tênis de Mesa";
      break;
    case "1":
      return "Crash";
      break;
    case "2":
      return "Double";
      break;
    default:
      break;
  }
};

const markets = {
  ml_ft: "Vencedor da Partida",
  ml_ht: "Vencedor (HT)",
  ml_gm: "Vencedor do Set",
  ml_qt: "Vencedor do Quarto",
  ah_ft: "Handicap Asiático (FT)",
  ah_ht: "Handicap Asiático (HT)",
  ah_gm: "Handicap (Pontos no Set)",
  ah_sets_ft: "Handicap (Games)",
  double_ml_ft: "Dupla Hipótese",
  over_under_ft: "FT - Gols(Pontos)",
  over_under_ht: "HT - Gols(Pontos)",
  over_under_qt: "Pontos no quarto",
  over_under_gm: "Set - Pontos",
  asian_over_under_ft: "Gols +/- (FT)",
  asian_over_under_ht: "Gols +/- (HT)",
  over_under_ft_player_a: "Gols Jogador 1",
  over_under_ft_player_b: "Gols Jogador 2",
  next_goal: "Próximo Gol",
  btts_ft: "Ambos Marcam (FT)",
  btts_ht: "Ambos Marcam (HT)",
  odd_even_ft: "Par ou Impar (FT)",
  odd_even_ht: "Par ou Impar (HT)",
  ml_btts_ft: "Resultado / Ambos Marcam (FT)",
  ml_btts_ht: "Resultado / Ambos Marcam (HT)",
};
