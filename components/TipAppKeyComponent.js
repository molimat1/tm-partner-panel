import { Box, Button, CircularProgress, Typography } from "@material-ui/core";
import axios from "axios";
import React from "react";

export default function TipAppKeyComponent({ code, mutate }) {
  //loading
  const [loading, setLoading] = React.useState(false);

  return (
    <Box
      p={1}
      bgcolor={"#333"}
      border={1}
      style={{
        color: "white",
        borderRadius: 5,
        borderColor: "#777",
      }}
    >
      <Typography
        style={{
          fontSize: 12,
          fontWeight: "bold",
        }}
      >
        Chave de teste do TipChat:{" "}
      </Typography>
      <Box display={"flex"} alignItems={"center"} sx={{ gap: 10 }}>
        {code}
        <Box
          sx={{
            cursor: "pointer",
            "&:hover": {
              opacity: 0.8,
            },
          }}
          onClick={() => {
            navigator.clipboard.writeText(code);
          }}
        >
          <svg
            style={{
              height: 25,
              width: 25,
            }}
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            fill="none"
            viewBox="0 0 24 24"
          >
            <path
              stroke="currentColor"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M14 4v3a1 1 0 0 1-1 1h-3m4 10v1a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V9a1 1 0 0 1 1-1h2m11-3v10a1 1 0 0 1-1 1h-7a1 1 0 0 1-1-1V7.87a1 1 0 0 1 .24-.65l2.46-2.87a1 1 0 0 1 .76-.35H18a1 1 0 0 1 1 1Z"
            />
          </svg>
        </Box>
      </Box>
      <Button
        style={{
          padding: 0,
          color: "red",
          fontSize: 12,
          display: "flex",

          justifyContent: "center",
          width: "100%",
        }}
        onClick={async (e) => {
          setLoading(true);
          try {
            await axios.post(
              ` https://api.tipmanager.net:443/v1/chat/unpair`,
              { id_slot: code },
              {
                headers: {
                  "Content-Type": "application/json",
                  Authorization: `Bearer ${process.env.TIP_MANAGER_TOKEN}`,
                },
              }
            );
            await mutate(e);
          } catch (err) {
            console.log(err);
          }
          setLoading(false);
        }}
      >
        <Typography style={{ fontSize: 12 }}>Resetar</Typography>
        <Box>
          <CircularProgress
            size={12}
            color={"inherit"}
            style={{
              display: loading ? "block" : "none",
              marginLeft: 10,
            }}
          />
        </Box>
      </Button>
    </Box>
  );
}
