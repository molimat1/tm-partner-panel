import React, { useState } from "react";
import {
    FormControl,
    TextField,
    Button,
    Typography,
    LinearProgress,
} from "@material-ui/core";

import { makeStyles } from "@material-ui/core/styles";

import { useUser } from "./hooks/UserContext";

const useStyles = makeStyles((theme) => ({
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        padding: theme.spacing(4),
    },

    submit: {
        margin: theme.spacing(8, 0, 2),
        backgroundColor: theme.palette.success.light,
    },
    text: {
        textAlign: "center",
    },
}));

const initialFValues = {
    email: "",
};

const PasswordReset = () => {
    const { passReset } = useUser();
    const classes = useStyles();

    const [values, setValues] = useState(initialFValues);
    const [errors, setErrors] = useState({});
    const [message, setMessage] = useState("");
    const [loading, setLoading] = useState(false);

    const resetPassword = async (info = values) => {
        setLoading(true);
        setMessage("");
        const { error, data } = await passReset(values.email);

        setMessage("Verifique seu e-mail para redefinir sua senha");

        setLoading(false);
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setValues({
            ...values,
            [name]: value,
        });
    };

    const validate = (fieldValues = values) => {
        let temp = { ...errors };

        if ("email" in fieldValues) {
            temp.email = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
                fieldValues.email
            )
                ? ""
                : "Por favor, digite seu e-mail.";
        }

        setErrors({
            ...temp,
        });

        if (fieldValues === values)
            return Object.values(temp).every((x) => x == "");
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (validate()) {
            resetPassword();
        }
    };

    return (
        <FormControl>
            <form className={classes.form} noValidate onSubmit={handleSubmit}>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    value={values.email}
                    autoFocus
                    onChange={handleInputChange}
                    fullWidth
                    id="email"
                    label="E-mail"
                    name="email"
                    autoComplete="email"
                    error={errors.email}
                    helperText={errors.email}
                    disabled={loading}
                />

                <Typography
                    style={{ color: "#46a843" }}
                    variant="caption"
                    display="block"
                    align="center"
                >
                    {message}
                </Typography>

                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    disabled={loading}
                >
                    Redefinir minha senha
                </Button>
            </form>
            {loading && <LinearProgress />}
        </FormControl>
    );
};

export default PasswordReset;
