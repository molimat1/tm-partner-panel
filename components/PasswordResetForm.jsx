import React from "react";

import {
    Container,
    Paper,
    Modal,
    Backdrop,
    Typography,
    Box,
    Link,
    Avatar,
    Grow,
} from "@material-ui/core";

import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import { makeStyles, fade } from "@material-ui/core/styles";

import PasswordForget from "./PasswordForget";
import { useAuthFlow } from "./hooks/AuthFlow";

function Copyright() {
    return (
        <Typography variant="body2" color="secondary" align="center">
            {"Copyright © "}
            <Link color="secondary" href="https://tipmanager.bet/">
                Tip Manager
            </Link>{" "}
            {new Date().getFullYear()}
            {"."}
        </Typography>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        margin: theme.spacing(0),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        backgroundColor: fade(theme.palette.common.white, 1),
    },
    avatar: {
        margin: theme.spacing(2),
        backgroundColor: theme.palette.success.light,
    },

    modal: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
}));

export default function PasswordResetForm() {
    const classes = useStyles();
    const { toggleResetPass } = useAuthFlow();

    return (
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={Boolean(toggleResetPass)}
            onClose={toggleResetPass}
            className={classes.modal}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}
        >
            <Grow in={true}>
                <Container component="main" maxWidth="xs">
                    <Paper className={classes.paper} elevation={10}>
                        <Avatar className={classes.avatar}>
                            <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Forneça seu e-mail
                        </Typography>
                        <PasswordForget />
                    </Paper>
                    <Box mt={8}>
                        <Copyright />
                    </Box>
                </Container>
            </Grow>
        </Modal>
    );
}
