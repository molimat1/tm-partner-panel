import React, { useState, useEffect } from "react";

import axios from "axios";
import moment from "moment";
import {
  FormControl,
  TextField,
  Button,
  Typography,
  Grid,
  Link,
  CircularProgress,
  Box,
  Select,
  MenuItem,
} from "@material-ui/core";

import { makeStyles, withStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  form: {
    display: "flex",
    flexDirection: "column",
  },
  submit: {
    margin: theme.spacing(2, 0, 2),
    width: "100px",
    backgroundColor: "#0c9",
    color: "#fcfcfc",
    "&:hover": {
      color: "#222",
    },
    transition: "all .4s",
  },
  select: {
    "& ul": {
      backgroundColor: "#FFF",
    },
    "& li": {
      fontSize: 14,
    },
  },
  textField: {
    width: "300px",
    paddingBottom: 0,

    fontWeight: 500,
  },
  input: {
    backgroundColor: "#ececec",
    height: 50,
    marginRight: 20,
    color: "#000",
    "&:hover": {
      color: "#fff",
    },
  },
  inputBox: {
    width: "100%",
    height: "20%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    width: "100%",
    height: "450px",
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
  },
}));

const initialFValues = {
  name: "",
};

const ValidationTextField = withStyles({
  root: {
    "& .MuiFormLabel-root.Mui-focused": {
      color: "#0c9",
    },

    "& .MuiFilledInput-underline::after": {
      borderBottom: "2px solid #0c9",
    },
    "& .Mui-focused": {
      color: "#fff",
    },
  },
})(TextField);

const LoginFormFields = () => {
  const classes = useStyles();

  const [email, setEmail] = useState("");
  const [offer, setOffer] = useState("bot");
  const [message, setMessage] = useState("");
  const [info, setInfo] = useState("");
  const [loading, setLoading] = useState(false);

  const resetForm = () => {
    setEmail("");
  };

  const createTrial = async (email, offer) => {
    setLoading(true);

    const trialUser = {
      email: email.replaceAll(" ", "").toLowerCase(),
      offer,
    };

    if (email && offer) {
      try {
        const { data } = await axios.post("/api/createTrial", trialUser);
        console.log("data link", data);
        setInfo(data?.link);
        setMessage("");
      } catch (e) {
        setInfo("");

        setMessage(e.response.data);
      }
    } else {
      setMessage("Preencha os campos!");
    }
    setLoading(false);
    resetForm();
  };

  return (
    <div className={classes.container}>
      <div className={classes.form}>
        <ValidationTextField
          variant="filled"
          margin="normal"
          required
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          autoFocus
          id="email"
          label="E-mail"
          name="email"
          disabled={loading}
          className={classes.textField}
          InputProps={{
            className: classes.input,
          }}
        />
        <Typography style={{ color: "white" }}>Plano</Typography>
        <Box display="flex" style={{ gap: 10 }} alignItems="center">
          <Select
            variant="outlined"
            style={{ backgroundColor: "#ececec", width: "280px" }}
            fullWidth
            value={offer}
            onChange={(e) => setOffer(e.target.value)}
          >
            <MenuItem value="casino">CASSINO</MenuItem>
            <MenuItem value="pro">PRO</MenuItem>
            <MenuItem value="live">LIVE</MenuItem>
            <MenuItem value="bot">BOT</MenuItem>
            <MenuItem value="soccer-live">SOCCER LIVE</MenuItem>
            <MenuItem value="soccer-bot">SOCCER BOT</MenuItem>
            <MenuItem value="tipster">TIPSTER</MenuItem>
          </Select>
        </Box>

        <Box style={{ display: "flex", justifyContent: "center" }}>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            className={classes.submit}
            disabled={loading}
            onClick={() => createTrial(email, offer)}
          >
            Criar
          </Button>
        </Box>
      </div>
      <div
        style={{
          width: "100%",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {loading ? (
          <div>
            <CircularProgress style={{ color: "#0c9", marginLeft: 20 }} />{" "}
            <Typography align="center" style={{ color: "white" }}>
              Carregando
            </Typography>
          </div>
        ) : (
          <Box>
            {message ? (
              <Typography align="center" style={{ color: "white" }}>
                {message}
              </Typography>
            ) : (
              <Box
                style={{
                  width: "100%",
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                {info && (
                  <Typography
                    align="center"
                    style={{ color: "#0c9", maxWidth: 300, overflow: "auto" }}
                  >
                    {info}
                  </Typography>
                )}
                <Button
                  fullWidth
                  variant="contained"
                  className={classes.submit}
                  disabled={loading}
                  onClick={() =>
                    //copyToClipboard(info)
                    navigator.clipboard.writeText(info)
                  }
                >
                  Copiar
                </Button>
              </Box>
            )}
          </Box>
        )}
      </div>
    </div>
  );
};

export default LoginFormFields;
