import React, { useState, useEffect } from "react";
import axios from "axios";
import { FormControl, TextField, Button, Typography, Grid, Link, LinearProgress } from "@material-ui/core";

import { makeStyles, withStyles } from "@material-ui/core/styles";

import { useRouter } from "next/router";
import { useUser } from "./hooks/UserContext";
import { useAuthFlow } from "./hooks/AuthFlow";

const useStyles = makeStyles((theme) => ({
    form: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
    },
    submit: {
        margin: theme.spacing(4, 0, 4),
        backgroundColor: "#0c9",
        color: "#fcfcfc",
        "&:hover": {
            color: "#222",
        },
        transition: "all .4s",
    },
    textField: {
        width: "20vw",
        [theme.breakpoints.down("sm")]: {
            width: "80vw",
        },
        marginLeft: "auto",
        marginRight: "auto",
        paddingBottom: 0,
        marginTop: 20,
        fontWeight: 500,
    },
    input: {
        backgroundColor: "#ececec",
        color: "#000",
        "&:hover": {
            color: "#fff",
        },
    },
}));

const initialFValues = {
    email: "",
    password: "",
};

const ValidationTextField = withStyles({
    root: {
        "& .MuiFormLabel-root.Mui-focused": {
            color: "#0c9",
        },

        "& .MuiFilledInput-underline::after": {
            borderBottom: "2px solid #0c9",
        },
        "& .Mui-focused": {
            color: "#fff",
        },
    },
})(TextField);

const LoginFormFields = () => {
    const { user, signIn, passReset } = useUser();

    const [values, setValues] = useState(initialFValues);
    const [errors, setErrors] = useState({});
    const [message, setMessage] = useState("");
    const [loading, setLoading] = useState(false);
    const [token, setToken] = useState("");
    const [toggleResetPassword, setToggleResetPassword] = useState(false);

    const router = useRouter();

    const loginUser = async () => {
        setLoading(true);
        setMessage("");

        const { error, user } = await signIn({
            email: values.email.toLowerCase(),
            password: values.password,
        });

        if (error) {
            setMessage("Email ou senha inválidos.");
        } else {
            router.push("/dashboard");
        }
        resetForm();
        setLoading(false);
    };

    const resetPassword = async (e) => {
        e.preventDefault();
        resetForm();
        setLoading(true);
        setMessage("");
        await passReset(values.email);
        setMessage("Verifique seu e-mail para redefinir sua senha");
        setLoading(false);
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setValues({
            ...values,
            [name]: value,
        });
    };

    const resetForm = () => {
        setValues(initialFValues);
        setErrors({});
    };

    const validate = (fieldValues = values) => {
        let temp = { ...errors };

        if ("email" in fieldValues) {
            temp.email = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(fieldValues.email) ? "" : "Por favor, digite seu e-mail.";
        }
        if ("password" in fieldValues) {
            temp.password = fieldValues.password.length > 7 ? "" : "Sua senha deve ter pelo menos 8 caracteres.";
        }

        setErrors({
            ...temp,
        });

        if (fieldValues === values) return Object.values(temp).every((x) => x == "");
    };

    const handleSubmit = React.useCallback(async (e) => {
        e.preventDefault();
        if (validate()) {
            setMessage("Acessando...");
            loginUser();
        }
    });

    const toggleReset = () => {
        setMessage("");
        setToggleResetPassword((value) => !value);
    };

    const classes = useStyles();

    if (toggleResetPassword) {
        return (
            <FormControl>
                <form className={classes.form} noValidate onSubmit={resetPassword}>
                    <ValidationTextField
                        variant="filled"
                        margin="normal"
                        required
                        value={values.email}
                        onChange={handleInputChange}
                        autoFocus
                        id="email"
                        label="E-mail"
                        name="email"
                        autoComplete="email"
                        error={errors.email ? true : false}
                        helperText={errors.email}
                        disabled={loading}
                        className={classes.textField}
                        InputProps={{
                            className: classes.input,
                        }}
                    />
                    <Typography style={{ color: "#0c9" }} variant="caption" display="block" align="center">
                        {message}
                    </Typography>
                    <Button type="submit" fullWidth variant="contained" className={classes.submit} disabled={loading}>
                        Redefinir minha senha
                    </Button>
                    <Link href="#" variant="body2" onClick={toggleReset} style={{ color: "red" }}>
                        Voltar
                    </Link>
                </form>
                {loading && <LinearProgress />}
            </FormControl>
        );
    } else {
        return (
            <FormControl>
                <form className={classes.form} noValidate onSubmit={handleSubmit}>
                    <ValidationTextField
                        variant="filled"
                        margin="normal"
                        required
                        value={values.email}
                        onChange={handleInputChange}
                        autoFocus
                        id="email"
                        label="E-mail"
                        name="email"
                        autoComplete="email"
                        error={errors.email ? true : false}
                        helperText={errors.email}
                        disabled={loading}
                        className={classes.textField}
                        InputProps={{
                            className: classes.input,
                        }}
                    />
                    <ValidationTextField
                        variant="filled"
                        margin="normal"
                        required
                        fullWidth
                        value={values.password}
                        onChange={handleInputChange}
                        name="password"
                        label="Senha"
                        id="password"
                        type="password"
                        error={errors.password ? true : false}
                        helperText={errors.password}
                        disabled={loading}
                        className={classes.textField}
                        InputProps={{
                            className: classes.input,
                        }}
                    />
                    <Typography color="error" variant="caption" display="block" align="center">
                        {message}
                    </Typography>

                    <Button type="submit" fullWidth variant="contained" className={classes.submit} disabled={loading}>
                        Entrar
                    </Button>

                    <Link href="#" variant="body2" onClick={toggleReset} style={{ color: "white" }}>
                        Esqueci minha senha
                    </Link>
                </form>
                {loading && <LinearProgress />}
            </FormControl>
        );
    }
};

export default LoginFormFields;
