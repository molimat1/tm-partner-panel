import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";

import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

import { getFormattedDate } from "../../utils/dateHelper";

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        flexWrap: "wrap",
        marginLeft: 5,
        marginRight: 5,
    },
    paper: {
        padding: "6px 10px 6px",
        width: "100%",
        minHeight: "150px",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        "&:hover": {
            scale: 1.05,
            cursor: "pointer",
        },
        transition: "all .2s",
    },
    paperRows: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "flex-end",
    },
}));

const getStatus = (status, cancelAtEnd) => {
    let text,
        color = "";

    switch (status) {
        case "active":
            text = "Ativo";
            color = "#0c9";
            break;
        case "canceled":
            text = "Cancelado";
            color = "#c20";
            break;

        default:
            text = "Indisponível";
            color = "#444";
            break;
    }

    if (cancelAtEnd) {
        text = "Ativo até fim do ciclo";
        color = "#c90";
    }

    return { text, color };
};

const getInterval = (value) => {
    let interval = "";
    switch (value) {
        case 1:
            interval = "Mensal";
            break;
        case 3:
            interval = "Trimestral";
            break;
        case 6:
            interval = "Semestral";
            break;
        case 12:
            interval = "Anual";
            break;

        default:
            break;
    }

    return interval;
};

export default function SimplePaper({ data }) {
    const classes = useStyles();
    const theme = useTheme();
    const { text, color } = getStatus(data.status, data.cancelAtEnd);
    const interval = getInterval(data.interval);
    const smUp = useMediaQuery(theme.breakpoints.up("sm"), { noSsr: true });

    return (
        <div className={classes.root}>
            <Paper elevation={2} className={classes.paper} onClick={() => {}}>
                <div
                    style={{
                        width: "100%",
                        height: 6,
                        marginBottom: -15,
                        marginTop: 5,
                        backgroundColor: color,
                    }}
                />
                <div className={classes.paperRows} style={{ alignItems: "flex-start", marginTop: 10 }}>
                    <div>
                        <Typography
                            variant={"body"}
                            style={{
                                textTransform: "uppercase",
                                fontWeight: 600,
                                fontSize: smUp ? 16 : 12,
                            }}>
                            {data.email}
                        </Typography>
                        <Typography variant={"body2"} style={{ fontSize: smUp ? 14 : 12 }}>
                            {" "}
                            {data.name ? data.name : " Pago via PIX ou Boleto"}
                        </Typography>
                    </div>
                    <Typography
                        variant={"subtitle2"}
                        align="right"
                        style={{
                            color: color,
                            width: 80,
                            fontSize: 12,
                            fontWeight: 600,
                        }}>
                        {text}
                    </Typography>
                </div>
                <div className={classes.paperRows}>
                    <div>
                        <Typography variant={"body2"}>{data.id}</Typography>
                    </div>
                </div>
                <div className={classes.paperRows}>
                    <div>
                        <Typography variant={"body2"} align="left" style={{ fontSize: smUp ? 14 : 12 }}>
                            Cliente {interval}
                        </Typography>
                        <Typography variant={"body2"} align="left" style={{ fontSize: smUp ? 12 : 10 }}>
                            Próximo ciclo a partir de <strong>{getFormattedDate(data.periodEnd)}</strong>
                        </Typography>
                    </div>
                    <div>
                        <Typography
                            variant="subtitle2"
                            align="right"
                            style={{
                                fontWeight: 600,
                                fontSize: smUp ? 14 : 12,
                            }}>
                            Total Gasto
                        </Typography>
                        <Typography variant={"h6"} align="right" style={{ lineHeight: 1, fontSize: 28 }}>
                            <span style={{ fontSize: 14 }}>R$</span> {String(data.amountPaid / 100).replace(".", ",")}
                        </Typography>
                    </div>
                </div>
            </Paper>
        </div>
    );
}
