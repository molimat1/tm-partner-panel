require("dotenv").config();

const nextConfiguration = {
  //will output independent pages that don't require a monolithic server. It's only compatible with next start or Serverless deployment platforms (like ZEIT Now) — you cannot use the custom server API.
  env: {
    TIP_MANAGER_TOKEN: process.env.TIP_MANAGER_TOKEN,
    SENDGRID_API_KEY: process.env.SENDGRID_API_KEY,
    STRIPE_SECRET_KEY: process.env.STRIPE_SECRET_KEY,
    STRIPE_WEBHOOK_SECRET: process.env.STRIPE_WEBHOOK_SECRET,
    NEXT_PUBLIC_STRIPE_PUBLISHABLE_KEY: process.env.NEXT_PUBLIC_STRIPE_PUBLISHABLE_KEY,
    FIREBASE_API_KEY: process.env.FIREBASE_API_KEY,
    FIREBASE_DOMAIN: process.env.FIREBASE_DOMAIN,
    FIREBASE_DB_URI: process.env.FIREBASE_DB_URI,
    FIREBASE_CLIENT_EMAIL: process.env.FIREBASE_CLIENT_EMAIL,
    NEXT_PUBLIC_FIREBASE_PROJECT_ID: process.env.NEXT_PUBLIC_FIREBASE_PROJECT_ID,
    FIREBASE_PRIVATE_KEY: process.env.FIREBASE_PRIVATE_KEY,
    NEXT_PUBLIC_SUPABASE_URL: process.env.NEXT_PUBLIC_SUPABASE_URL,
    NEXT_PUBLIC_SUPABASE_ANON_KEY: process.env.NEXT_PUBLIC_SUPABASE_ANON_KEY,
    SUPABASE_SERVICE_ROLE_KEY: process.env.SUPABASE_SERVICE_ROLE_KEY,
    NEXT_PUBLIC_CONTENTFUL_SPACE_ID: process.env.NEXT_PUBLIC_CONTENTFUL_SPACE_ID,
    NEXT_PUBLIC_CONTENTFUL_ACCESS_TOKEN: process.env.NEXT_PUBLIC_CONTENTFUL_ACCESS_TOKEN,
    MAGICBELL_API_KEY: process.env.MAGICBELL_API_KEY,
    MAGICBELL_API_SECRET: process.env.MAGICBELL_API_SECRET,
    SECRET_CAPTCHA: process.env.SECRET_CAPTCHA,
    SUBSCRIPTION_ESPORT: process.env.SUBSCRIPTION_ESPORT,
    SUBSCRIPTION_ESPORT_LIVE: process.env.SUBSCRIPTION_ESPORT_LIVE,
    MAILCHIMP_UKEY: process.env.MAILCHIMP_UKEY,
    TIP_MANAGER_API_VERSION: process.env.TIP_MANAGER_API_VERSION,
    GERENCIANET_ID: process.env.GERENCIANET_ID,
    GERENCIANET_KEY: process.env.GERENCIANET_KEY,
    GERENCIANET_NOTIFICATION_URL: process.env.GERENCIANET_NOTIFICATION_URL,
    GERENCIANET_PIX_ENDPOINT: process.env.GERENCIANET_PIX_ENDPOINT,
    GERENCIANET_CHAVE_PIX: process.env.GERENCIANET_CHAVE_PIX,
    GERENCIANET_PIX_CERTIFICATE: process.env.GERENCIANET_PIX_CERTIFICATE,
    STRIPE_DEFAULT_PRICE_ID: process.env.STRIPE_DEFAULT_PRICE_ID,
  },

  webpack: (config, { webpack, dev, buildId }) => {
    config.plugins.push(
      new webpack.DefinePlugin({
        "process.env.BUILD_ID": JSON.stringify(dev ? "development" : buildId),
      })
    );

    config.module.rules.push({
        test: /\.html$/i,
        loader: "html-loader",
      });
      config.module.rules.push({
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },);

      
    return config;
  },
  



  serverRuntimeConfig: {
    PROJECT_ROOT: __dirname,
  },
};

module.exports = nextConfiguration;
