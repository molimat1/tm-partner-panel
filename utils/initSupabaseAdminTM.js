import { createClient } from "@supabase/supabase-js";

export const supabaseAdminTM = createClient(process.env.NEXT_PUBLIC_SUPABASE_URL_TM, process.env.SUPABASE_SERVICE_ROLE_KEY_TM);
