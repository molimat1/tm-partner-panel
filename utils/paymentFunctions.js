import moment from "moment";
import { LocalAtm, EventNote, TrendingDown, TrendingUp } from "@material-ui/icons";

export const getWindowSum = (invoices, window) => {
    const startDate = moment(window[0], "DD-MM-YY").valueOf() / 1000; // conver to unix timee
    const endDate = moment(window[1], "DD-MM-YY").valueOf() / 1000; // conver to unix timee

    let sum = 0;
    invoices.map((invoice) => {
        if (invoice.created > startDate) {
            if (invoice.created <= endDate) {
                sum = sum + invoice.amountPaid;
            }
        }
    });

    sum = sum / 100;

    return sum.toFixed(2);
};

export const getWindowCount = (invoices, window) => {
    const startDate = moment(window[0], "DD-MM-YY").valueOf() / 1000; // conver to unix timee
    const endDate = moment(window[1], "DD-MM-YY").valueOf() / 1000; // conver to unix timee

    let count = 0;
    invoices.map((invoice) => {
        if (invoice.created > startDate) {
            if (invoice.created <= endDate) {
                count = count + 1;
            }
        }
    });

    return count;
};

export const getNewSubscribers = (invoices, window) => {
    // This function will check wether the invoice is from a new customer or not and than will push this invoice to
    // a list which returns new customers between start and end date

    const startDate = moment(window[0], "DD-MM-YY").valueOf() / 1000; // conver to unix timee
    const endDate = moment(window[1], "DD-MM-YY").valueOf() / 1000; // conver to unix timee

    let customersList = {};

    let count = 0;
    invoices.map((invoice) => {
        if (invoice.created > startDate) {
            if (invoice.created <= endDate) {
                if (!customersList[invoice?.customerId] && invoice?.created >= startDate && invoice?.created < endDate)
                    customersList[invoice?.customerId] = invoice?.created;
            }
        }
    });

    return customersList;
};

export function getMonthNewSubscribers(monthDays, invoices) {
    let chartData = [];
    let total = 0;
    let growth = 0;
    let today = moment();
    let lastMonth = [today.subtract(1, "months").format("DD/MM/YY"), today.subtract(2, "months").format("DD/MM/YY")];

    for (let index = 0; index < monthDays.length - 1; index++) {
        const list = getNewSubscribers(invoices, [monthDays[index + 1], monthDays[index]]);
        const value = Object.keys(list).length;
        total = total + (value || 0);
        chartData.push({
            day: monthDays[index + 1],
            count: value,
        });
    }

    const lastMonthValue = Object.keys(getNewSubscribers(invoices, [lastMonth[1], lastMonth[0]])).length;

    // get changeRates in %
    if (lastMonthValue > total) {
        growth = -(1 - total / lastMonthValue) * 100;
    } else {
        if (lastMonthValue == total) {
            growth = 0;
        } else {
            growth = (total / lastMonthValue - 1) * 100;
        }
    }

    growth = growth.toFixed(1);
    total = total;
    chartData.reverse();
    return { chartData, total, growth };
}

export function getWeekValueChart(weekdays, invoices) {
    let chartData = [];
    let total = 0;
    let growth = 0;
    let today = moment();
    let lastWeek = [today.subtract(6, "days").format("DD/MM/YY"), today.subtract(7, "days").format("DD/MM/YY")];

    for (let index = 0; index < weekdays.length - 1; index++) {
        const value = getWindowSum(invoices, [weekdays[index + 1], weekdays[index]]);

        total = total + (parseFloat(value) || 0);
        chartData.push({
            day: weekdays[index + 1],
            count: value,
        });
    }

    const lastWeekValue = getWindowSum(invoices, [lastWeek[1], lastWeek[0]]);

    // get changeRates in %
    if (lastWeekValue > total) {
        growth = -(1 - total / lastWeekValue) * 100;
    } else {
        if (lastWeekValue == total) {
            growth = 0;
        } else {
            growth = (total / lastWeekValue - 1) * 100;
        }
    }

    growth = growth.toFixed(1);
    total = total.toFixed(2);
    chartData.reverse();
    return { chartData, total, growth };
}

export function getAnualInsight(invoices, customers) {
    const startDate = moment().startOf("year").format("DD/MM/YY");
    const endDate = moment().endOf("year").format("DD/MM/YY");

    const totalAmount = getWindowSum(invoices, [startDate, endDate]);
    const activeSubscribers = getActiveSubscribers(customers);
    const paymentsReceived = getWindowCount(invoices, [startDate, endDate]);
    const newSubs = Object.keys(getNewSubscribers(invoices, [startDate, endDate])).length;
    const churneys = getChurneys(customers);

    const anualInsightsData = [
        {
            icon: <TrendingUp />,
            value: newSubs,
            title: "Assinantes no Ano",
        },
        {
            icon: <EventNote />,
            value: activeSubscribers,
            title: "Assinantes Ativos",
        },
        {
            icon: <TrendingDown />,
            value: churneys,
            title: "Assinantes Não-Ativos",
        },
        {
            icon: <LocalAtm />,
            value: `R$ ${totalAmount}`,
            title: "Valor Total",
        },

        {
            icon: <LocalAtm />,
            value: paymentsReceived,
            title: "Pagamentos Recebidos",
        },
    ];

    return anualInsightsData;
}

export function getMonthValueChart(monthDays, invoices) {
    let chartData = [];
    let total = 0;
    let growth = 0;
    let today = moment();
    let lastMonth = [today.subtract(1, "months").format("DD/MM/YY"), today.subtract(2, "months").format("DD/MM/YY")];

    for (let index = 0; index < monthDays.length - 1; index++) {
        const value = getWindowSum(invoices, [monthDays[index + 1], monthDays[index]]);

        total = total + (parseFloat(value) || 0);
        chartData.push({
            day: monthDays[index + 1],
            count: value,
        });
    }

    const lastMonthValue = getWindowSum(invoices, [lastMonth[1], lastMonth[0]]);

    // get changeRates in %
    if (lastMonthValue > total) {
        growth = -(1 - total / lastMonthValue) * 100;
    } else {
        if (lastMonthValue == total) {
            growth = 0;
        } else {
            growth = (total / lastMonthValue - 1) * 100;
        }
    }

    growth = growth.toFixed(1);
    total = total.toFixed(2);
    chartData.reverse();
    return { chartData, total, growth };
}

export function getYearValueChart(invoices) {
    let chartData = [];
    let total = 0;

    let year = moment().get("year");

    for (let index = 1; index < 13; index++) {
        const startOfMonth = moment(`${year}-${index}-05`).clone().startOf("month").format("DD/MM/YY");
        const endOfMonth = moment(`${year}-${index}-05`).clone().endOf("month").format("DD/MM/YY");

        const value = getWindowSum(invoices, [startOfMonth, endOfMonth]);

        total = total + (parseFloat(value) || 0);
        chartData.push({
            month: monthName(index),
            value: parseFloat(value),
        });
    }

    total = total.toFixed(2);

    return { chartData, total };
}

export function getWeeklyPayments(weekdays, invoices) {
    let chartData = [];
    let total = 0;
    let growth = 0;
    let today = moment();
    let lastWeek = [today.subtract(6, "days").format("DD/MM/YY"), today.subtract(7, "days").format("DD/MM/YY")];

    for (let index = 0; index < weekdays.length - 1; index++) {
        const value = getWindowCount(invoices, [weekdays[index + 1], weekdays[index]]);
        total = total + (value || 0);
        chartData.push({
            day: weekdays[index + 1],
            count: value,
        });
    }

    const lastWeekValue = getWindowCount(invoices, [lastWeek[1], lastWeek[0]]);

    // get changeRates in %
    if (lastWeekValue > total) {
        growth = -(1 - total / lastWeekValue) * 100;
    } else {
        if (lastWeekValue == total) {
            growth = 0;
        } else {
            growth = (total / lastWeekValue - 1) * 100;
        }
    }

    growth = growth.toFixed(1);
    chartData.reverse();
    return { chartData, total, growth };
}

export const getActiveSubscribers = (customers) => {
    let totalActive = 0;
    Object.values(customers).map((item) => (item.status === "active" ? totalActive++ : null));
    return totalActive;
};

export const getChurneys = (customers) => {
    let totalActive = 0;
    Object.values(customers).map((item) => (item.status !== "active" ? totalActive++ : null));
    return totalActive;
};

const monthName = (number) => {
    switch (number) {
        case 1:
            return "Janeiro";
            break;
        case 2:
            return "Fevereiro";
            break;
        case 3:
            return "Março";
            break;
        case 4:
            return "Abril";
            break;
        case 5:
            return "Maio";
            break;
        case 6:
            return "Junho";
            break;
        case 7:
            return "Julho";
            break;
        case 8:
            return "Agosto";
            break;
        case 9:
            return "Setembro";
            break;
        case 10:
            return "Outubro";
            break;
        case 11:
            return "Novembro";
            break;
        case 12:
            return "Dezembro";
            break;

        default:
            break;
    }
};
