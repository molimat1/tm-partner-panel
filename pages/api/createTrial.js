import axios from "axios";

const createTrial = async (req, res) => {
  if (req.method === "POST") {
    const { email, offer } = req.body;

    const body = {
      account_token: process.env.DIGITALGURU_API_TOKEN,
      contact: {
        email,
      },
      blocked: true,
    };

    //Pegar informações necessárias do usuário
    try {
      const response = await axios.post(
        "https://digitalmanager.guru/api/checkout/offers/settings",
        body
      );

      console.log("response", response?.data);

      const link = getOfferLink(offer);

      const fullLink = link + "?settings=" + response?.data?.hash;

      const { data } = await axios.post(
        "https://bot.tipmanager.link/trial_short_url",
        { url: fullLink },
        {
          headers: {
            "x-api-key": "1988e18d-b2d2-49dc-bcc5-657209ad253d",
          },
        }
      );

      console.log("Data", data);

      return res.status(200).json({ link: data?.url });
    } catch (err) {
      console.log(err?.raw?.message || err.message);

      return res.status(500).end(err?.raw?.message || err.message);
    }
  } else {
    res.setHeader("Allow", "POST");
    res.status(405).end("Method Not Allowed");
  }
};

const getOfferLink = (offer) => {
  switch (offer) {
    case "bot":
      return "https://checkout.tipmanager.net/subscribe/9a296ec6-9bd5-456a-aa83-407b2ecc2942";
    case "live":
      return "https://checkout.tipmanager.net/subscribe/9a296e66-15b5-4a36-88cc-157a0f752a85";
    case "pro":
      return "https://checkout.tipmanager.net/subscribe/9a2970b4-fea6-47f2-9524-2b7e167862ff";
    case "casino":
      return "https://checkout.tipmanager.net/subscribe/teste-casino";
    case "tipster":
      return "https://checkout.tipmanager.net/subscribe/99e3de40-4182-4b8a-9cca-94ec8a1cf61c";
    case "soccer-live":
      return "https://checkout.tipmanager.net/subscribe/9a9e31f4-bbf6-4b6b-a582-4105477ea5d4";
    case "soccer-bot":
      return "https://checkout.tipmanager.net/subscribe/9a9e31c1-cf3b-4e27-a88b-ee0166cb2745";
    default:
      break;
  }
};

export default createTrial;
