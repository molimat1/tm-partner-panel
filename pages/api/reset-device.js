import axios from "axios";

const getUserData = async (req, res) => {
  if (req.method === "GET") {
    const { email, bot } = req.query;

    try {
      console.log(email, bot);
      const TIP_MANAGER_TOKEN = process.env.TIP_MANAGER_TOKEN;

      console.log(TIP_MANAGER_TOKEN);
      if (bot === "esports") {
        console.log("resetando para sports", email, bot);
        const res1 = await axios.post(
          `https://api.tipmanager.net:443/v1/bot/reset_pair?email=${email}`,
          {},
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${TIP_MANAGER_TOKEN}`,
            },
          }
        );
        console.log(res1);
      }

      if (bot === "casino") {
        const res2 = await axios.post(
          `https://api.tipmanager.net:443/v1/bot_casino/reset_pair?email=${email}`,
          {},
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${TIP_MANAGER_TOKEN}`,
            },
          }
        );
        console.log(res2);
      }

      return res.status(200).end();
    } catch (err) {
      console.log(err);
      console.log(err?.raw?.message || err.message);

      return res.status(500).json({
        info: err?.raw?.message || err.message,
      });
    }
  } else {
    res.setHeader("Allow", "GET");
    res.status(405).end("Method Not Allowed");
  }
};

export default getUserData;
