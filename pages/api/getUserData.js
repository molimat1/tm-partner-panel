import axios from "axios";
import { stripe } from "../../utils/initStripe";
import { supabaseAdminTM } from "../../utils/initSupabaseAdminTM";

const { Pool, Client } = require("pg");

const client = new Pool({
  user: "postgres.jnurxezspleufiooyekw",
  host: "aws-0-sa-east-1.pooler.supabase.com",
  database: "postgres",
  password: process.env.SUPABASE_POSTGRES_PASSWORD,
  port: 5432,
});

const getUserData = async (req, res) => {
  if (req.method === "GET") {
    const { email } = req.query;

    console.log("Email 2", email);

    try {
      const { rows } = await client.query(
        `SELECT * FROM auth.users WHERE auth.users.email = '${email}'`
      );

      if (!rows.length || rows.length > 1)
        return res.status(404).json({ message: "User Not found" });

      if (!rows[0].email_confirmed_at)
        return res.status(405).json({ message: "User not confirmed" });

      const { id } = rows[0];

      const customer = await client.query(
        `SELECT * FROM public.customers c WHERE c.id = '${id}'`
      );

      const customers_guru = await client.query(
        `SELECT * FROM public.customers_guru c WHERE c.id = '${id}'`
      );

      const stripeId = customer?.rows?.[0]?.stripe_customer_id;
      const guruId = customers_guru?.rows?.[0]?.guru_id;

      console.log("guruId", guruId);
      const TIP_MANAGER_TOKEN = process.env.TIP_MANAGER_TOKEN;

      const res1 = axios.get(
        `https://api.tipmanager.net:443/v1/portal/list?id_user=${id}`,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${TIP_MANAGER_TOKEN}`,
          },
        }
      );

      const res3 = client.query(
        `SELECT * FROM public.credits c WHERE c.user_id = '${id}'`
      );

      const res4 = client.query(
        `SELECT
        s.*,
        COALESCE(prod.name, prod2.name) as name
      FROM
        public.subscriptions s
        LEFT JOIN public.prices p ON p.id = s.price_id
        LEFT JOIN public.products prod ON prod.id = p.product_id
        LEFT JOIN public.products prod2 ON s.product_id = prod2.id
      WHERE
        s.user_id = '${id}'`
      );

      let returnedData;
      if (stripeId) {
        const res5 = stripe.customers.retrieve(stripeId);

        const items = await Promise.all([res1, res3, res4, res5]);

        returnedData = {
          bots: items[0].data,
          credits: items[1].rows,
          subs: items[2].rows,
          id,
          stripeInfo: items[3],
          guruId,
        };
      } else {
        const items = await Promise.all([res1, res3, res4]);

        returnedData = {
          bots: items[0].data,
          credits: items[1].rows,
          subs: items[2].rows,
          id,
          guruId,
        };
      }

      return res.status(200).json(returnedData);
    } catch (err) {
      console.log("ERRO", err);
      console.log(err?.raw?.message || err.message);

      return res.status(500).json({
        info: err?.raw?.message || err.message,
      });
    }
  } else {
    res.setHeader("Allow", "GET");
    res.status(405).end("Method Not Allowed");
  }
};

export default getUserData;
