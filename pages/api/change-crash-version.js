import axios from "axios";

const getUserData = async (req, res) => {
  if (req.method === "GET") {
    const { email, version } = req.query;

    try {
      const TIP_MANAGER_TOKEN = process.env.TIP_MANAGER_TOKEN;

      
      if (version === "v1") {

        const res1 = await axios.delete(
          `https://api.tipmanager.net:443/v1/casino/remove_new_system`,       
          {
            data: {email},
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${TIP_MANAGER_TOKEN}`,
            },
          }
        );
        console.log(res1);
      }

      if (version === "v2") {
    
        const res1 = await axios.post(
          `https://api.tipmanager.net:443/v1/casino/insert_new_system`,
          {email},
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${TIP_MANAGER_TOKEN}`,
            },
          }
        );
        console.log(res1);
      }

      return res.status(200).end();
    } catch (err) {
      console.log(err)
      console.log(err?.raw?.message || err.message);

      return res.status(500).json({
        info: err?.raw?.message || err.message,
      });
    }
  } else {
    res.setHeader("Allow", "GET");
    res.status(405).end("Method Not Allowed");
  }
};

export default getUserData;
