import { supabaseAdminTM } from "../../utils/initSupabaseAdminTM";

const createUser = async (req, res) => {
  if (req.method === "GET") {
    const { data: user, error } =
      await supabaseAdminTM.auth.api.inviteUserByEmail(req?.query?.email);

    console.log(user, error);
    if (error) throw new Error("Problema gerando link");

    return res.status(200).json({ user });
  } else {
    res.setHeader("Allow", "GET");
    res.status(405).end("Method Not Allowed");
  }
};

export default createUser;
