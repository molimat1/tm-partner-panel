const excel = require("node-excel-export");

const { Duplex } = require("stream");

function bufferToStream(myBuuffer) {
  let tmp = new Duplex();
  tmp.push(myBuuffer);
  tmp.push(null);
  return tmp;
}

const styles = {
  headerDark: {
    fill: {
      fgColor: {
        rgb: "aaaaaa",
      },
    },
    font: {
      bold: true,
      sz: 12,
    },
  },
  bold: {
    font: {
      bold: true,
    },
  },
};

const getAllSpecification = (name) => {
  return {
    created: {
      displayName: "Data do invoice", // <- Here you specify the column header
      headerStyle: styles.headerDark,
      width: 80,
    },

    customer_email: {
      displayName: "Cliente",
      headerStyle: styles.headerDark,
      width: 120,
    },
    amount_paid: {
      displayName: "Valor pago",
      headerStyle: styles.headerDark,
      width: 70,
    },
    amount_refunded: {
      displayName: "Valor devolvido",
      headerStyle: styles.headerDark,
      width: 60,
    },
    collection_method: {
      displayName: "Método de pagamento",
      headerStyle: styles.headerDark,
      width: 60,
    },
  };
};

const prepareExcelFile = (data, name, extra) => {
  let value = 0;
  data.forEach((item) => (value = value + parseInt(item.amount_paid) - parseInt(item.amount_refunded)));

  const window = data.length > 10 ? (data.length > 25 ? (data.length > 50 ? 0.4 : 0.3) : 0.25) : 0.2;
  const newArray = [
    ...data,
    {
      created: "Pix a receber",
      customer_email: extra.pix,
      amount_paid: "Nome do beneficiário",
      amount_refunded: extra.name,
      collection_method: `Total a receber (${window * 100}%): ` + (value / 100) * window,
    },
  ];

  return {
    name: name, // <- Specify sheet name (optional)
    specification: getAllSpecification(name), // <- Report specification
    data: newArray,
  };
};

const Export = async (req, res) => {
  // check if the user has the bot id
  const { query } = req;

  if (!query.data) return res.status(403).end("Bad request");

  const data = JSON.parse(query.data);

  let report;
  //export
  try {
    try {
      report = excel.buildExport(
        data.map((item) =>
          prepareExcelFile(item.invoices, item.email, {
            pix: item.pix,
            email: item.email,
            name: item.name,
          })
        )
      );
    } catch (e) {
      console.log(e);
    }

    res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    res.setHeader("Content-Disposition", `attachment; filename=payments.xlsx`);

    const stream = bufferToStream(report);
    stream.pipe(res);
  } catch (err) {
    console.log(err);
    return res.status(500).json({
      error: { statusCode: 500, message: err.message },
    });
  }
};

export default Export;
