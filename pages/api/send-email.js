import mailjet from "node-mailjet";

const sender = mailjet.connect(process.env.MJ_APIKEY_PUBLIC, process.env.MJ_APIKEY_PRIVATE);

export default async function handler(req, res) {
  if (req.method === "POST") {
    //first_name, last_name, email, telegram, payment_form, payment_account
    const { email_id, user_email, name } = req.body;

    if (!email_id || !user_email) return res.status(403).end("bad request");

    try {
      console.log("enviando email");
      await sender.post("send", { version: "v3.1" }).request({
        Messages: [
          {
            From: {
              Name: "Equipe TipManager",
              Email: "contato@tipmanager.net",
            },
            To: [
              {
                Email: user_email,
              },
            ],
            TemplateID: email_id,
            TemplateLanguage: true,
            TemplateErrorReporting: {
              Email: "contato@tipmanager.net",
              Name: "Transactional Error",
            },
            TemplateErrorDeliver: true,
            Variables: { name: name ? name : "!" },
          },
        ],
      });

      return res.status(200).json({ message: "ok" });
    } catch (err) {
      console.log(err);
      return res.status(401).json({
        statusCode: 401,
        message: err,
      });
    }
  } else {
    res.status(403).json({ message: "Forbidden" });
    // Handle any other HTTP method
  }
}
