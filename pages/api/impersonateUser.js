import { supabaseAdminTM } from "../../utils/initSupabaseAdminTM";

const getUserData = async (req, res) => {
  if (req.method === "GET") {
    const { data: user, error } = await supabaseAdminTM.auth.api.generateLink(
      "magiclink",
      req?.query?.email
    );

    console.log(
      "localhost:3000/api/auth/confirm?type=magiclink&token_hash=" +
        user.hashed_token
    );
    if (error) throw new Error("Problema gerando link");

    return res.status(200).json({
      link:
        "https://app.tipmanager.net/api/auth/confirm?type=magiclink&token_hash=" +
        user.hashed_token,
    });
  } else {
    res.setHeader("Allow", "GET");
    res.status(405).end("Method Not Allowed");
  }
};

export default getUserData;
