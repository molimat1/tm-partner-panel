import { supabaseAdminTM } from "../../utils/initSupabaseAdminTM";

const sendConfirmationEmail = async (req, res) => {
  if (req.method === "GET") {
    const { data: user, error } = await supabaseAdminTM.auth.api.generateLink(
      "recovery",
      req?.query?.email
    );

    if (error) throw new Error("Problema gerando link");

    return res.status(200).json({ link: user?.action_link });
  } else {
    res.setHeader("Allow", "GET");
    res.status(405).end("Method Not Allowed");
  }
};

export default sendConfirmationEmail;
