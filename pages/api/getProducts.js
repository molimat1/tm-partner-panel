const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

/* {orderId: 3454,
      customer: {
        name: 'Alisha Chinai',
        profile_pic: 'https://via.placeholder.com/150x150',
      },
      orderDate: getTodayDate(),
      deliveryDate: getTodayDate(),
      status: 'onHold',}*/

const getInvoices = async (req, res) => {
    if (req.method === "GET") {
        const { token } = req.headers;

        try {
            const products = await stripe.products.list({
                limit: 50,
            });

            res.setHeader("Cache-Control", "s-maxage=60, stale-while-revalidate");
            return res.status(200).json({ products });
        } catch (err) {
            res.status(500).json({
                error: { statusCode: 500, message: err.message },
            });
        }
    } else {
        res.setHeader("Allow", "POST");
        res.status(405).end("Method Not Allowed");
    }
};

export default getInvoices;
