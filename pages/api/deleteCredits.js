import axios from "axios";
import { stripe } from "../../utils/initStripe";
import { supabaseAdminTM } from "../../utils/initSupabaseAdminTM";

const { Pool, Client } = require("pg");

const client = new Pool({
  user: "postgres.jnurxezspleufiooyekw",
  host: "aws-0-sa-east-1.pooler.supabase.com",
  database: "postgres",
  password: process.env.SUPABASE_POSTGRES_PASSWORD,
  port: 5432,
});

const getUserData = async (req, res) => {
  if (req.method === "GET") {
    const { email } = req.query;

    console.log("Email", email);

    try {
      const { rows } = await client.query(
        `SELECT * FROM auth.users WHERE auth.users.email = '${email}'`
      );

      if (!rows.length || rows.length > 1)
        return res.status(404).json({ message: "User Not found" });

      if (!rows[0].email_confirmed_at)
        return res.status(405).json({ message: "User not confirmed" });

      const { id } = rows[0];

      await client.query(
        `UPDATE credits SET current_amount = 0 WHERE user_id = '${id}'`
      );

      return res.status(200).json({ success: "ok" });
    } catch (err) {
      console.log(err?.raw?.message || err.message);

      return res.status(500).json({
        info: err?.raw?.message || err.message,
      });
    }
  } else {
    res.setHeader("Allow", "GET");
    res.status(405).end("Method Not Allowed");
  }
};

export default getUserData;
