import { sub } from "date-fns";
import { stripe } from "../../utils/initStripe";
import { supabaseAdmin } from "../../utils/initSupabaseAdmin";

const { Pool, Client } = require("pg");

const client = new Pool({
  user: "postgres.jnurxezspleufiooyekw",
  host: "aws-0-sa-east-1.pooler.supabase.com",
  database: "postgres",
  password: process.env.SUPABASE_POSTGRES_PASSWORD,
  port: 5432,
});

const createTrial = async (req, res) => {
  if (req.method === "POST") {
    console.log(req.body);
    const { days, email, currency, price } = req.body;

    console.log("criando para email ", email, " por ", days, " dia(s).");

    try {
      const { rows } = await client.query(
        `SELECT id FROM auth.users WHERE auth.users.email = '${email}'`
      );

      if (!rows.length) return res.status(400).end("User not found!");

      const { id } = rows[0];

      const customer = await createOrRetrieveCustomer({
        uuid: id,
        email,
      });

      //get product id of the price id
      const { product } = await stripe.prices.retrieve(price);

      //get the product id from the price id and check if the user had a subscription with the same product id
      const { data: subscriptions } = await stripe.subscriptions.list({
        customer,
        status: "all",
        limit: 30,
      });

      // Check if the user already has a subscription with the same product ID
      const existingSubscription = subscriptions.find(
        (sub) => sub.items.data[0].price.product === product
      );

      if (existingSubscription)
        return res.status(401).end("User has already used this plan before!");

      await stripe.subscriptions.create({
        customer,
        items: [
          {
            price,
            quantity: 3,
          },
        ],
        trial_period_days: parseInt(days),

        currency,
      });

      return res.status(200).json({ info: "Trial criado com sucesso" });
    } catch (err) {
      console.log(err?.raw?.message || err.message);

      return res.status(500).end(err?.raw?.message || err.message);
    }
  } else {
    res.setHeader("Allow", "POST");
    res.status(405).end("Method Not Allowed");
  }
};

export const createOrRetrieveCustomer = async ({ uuid, email }) => {
  const { rows } = await client.query(
    `SELECT stripe_customer_id FROM customers WHERE id = '${uuid}'`
  );
  if (!rows.length) {
    // No customer record found, let's create one.
    const customerData = {
      metadata: {
        supabaseUUID: uuid,
      },
    };
    if (email) customerData.email = email;
    console.log("Creating stripe customer");
    const customer = await stripe.customers.create(customerData);

    // Now insert the customer ID into our Supabase mapping table.

    console.log(
      "Creating customer on supabase subscription table for UUID:",
      uuid,
      " STRIPE ID:",
      customer.id
    );
    client.query(
      `INSERT INTO customers VALUES ('${String(uuid)}', '${String(
        customer.id
      )}')`,
      (err, res) => {
        if (err)
          throw {
            message:
              "Algo deu errado na hora de associar o user na table subscriptions:",
            err,
          };
      }
    );

    return customer.id;
  }

  if (rows.length) {
    const { stripe_customer_id } = rows[0];
    return stripe_customer_id;
  }
};

export default createTrial;
