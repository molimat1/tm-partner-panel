import moment from "moment";
import { supabaseAdmin } from "../../utils/initSupabaseAdmin";

const { Pool, Client } = require("pg");

const client = new Pool({
  user: "postgres.jnurxezspleufiooyekw",
  host: "aws-0-sa-east-1.pooler.supabase.com",
  database: "postgres",
  password: process.env.SUPABASE_POSTGRES_PASSWORD,
  port: 5432,
});

const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

const excel = require("node-excel-export");

const { Duplex } = require("stream");

function bufferToStream(myBuuffer) {
  let tmp = new Duplex();
  tmp.push(myBuuffer);
  tmp.push(null);
  return tmp;
}

const styles = {
  headerDark: {
    fill: {
      fgColor: {
        rgb: "aaaaaa",
      },
    },
    font: {
      bold: true,
      sz: 12,
    },
  },
  bold: {
    font: {
      bold: true,
    },
  },
};

const getAllSpecification = (name) => {
  return {
    created: {
      displayName: "Data do invoice", // <- Here you specify the column header
      headerStyle: styles.headerDark,
      width: 80,
    },

    customer_email: {
      displayName: "Cliente",
      headerStyle: styles.headerDark,
      width: 120,
    },
    amount_paid: {
      displayName: "Valor pago",
      headerStyle: styles.headerDark,
      width: 70,
    },
    amount_refunded: {
      displayName: "Valor devolvido",
      headerStyle: styles.headerDark,
      width: 60,
    },
    collection_method: {
      displayName: "Método de pagamento",
      headerStyle: styles.headerDark,
      width: 60,
    },
  };
};

const prepareExcelFile = (data, name, extra) => {
  let value = 0;
  data.forEach(
    (item) =>
      (value =
        value + parseInt(item.amount_paid) - parseInt(item.amount_refunded))
  );

  const window =
    data.length > 10
      ? data.length > 25
        ? data.length > 50
          ? 0.4
          : 0.3
        : 0.25
      : 0.2;
  const newArray = [
    ...data,
    {
      created: "Pix a receber",
      customer_email: extra.pix,
      amount_paid: "Nome do beneficiário",
      amount_refunded: extra.name,
      collection_method:
        `Total a receber (${window * 100}%): ` + (value / 100) * window,
    },
  ];

  return {
    name: name, // <- Specify sheet name (optional)
    specification: getAllSpecification(name), // <- Report specification
    data: newArray,
  };
};

const getInvoices = async (req, res) => {
  if (req.method === "GET") {
    const { month, year } = req.query;

    const { data } = await supabaseAdmin.from("users").select("*");

    const payments = await Promise.all(
      data.map(async (item) => {
        const subscriptionsList = supabaseSubscriptions.map((item) => item.id);

        const { rows: couponInvoices } = await client.query(
          `SELECT * FROM invoices i WHERE i.subscription_id IN ('${subscriptionsList.join(
            "','"
          )} ') OR i.coupon = '${item.coupon}' `
        );

        const filtered = couponInvoices.filter((item) => {
          return (
            item.created.getMonth() === parseInt(month - 1) &&
            item.created.getFullYear() === parseInt(year)
          );
        });

        return {
          ...item,
          invoices: filtered,
        };
      })
    );

    try {
      const report = excel.buildExport(
        payments.map((item) =>
          prepareExcelFile(item.invoices, item.email, {
            pix: item.pix,
            email: item.email,
            name: item.name,
          })
        )
      );
      res.setHeader(
        "Content-Type",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      );
      res.setHeader(
        "Content-Disposition",
        `attachment; filename=payments.xlsx`
      );

      const stream = bufferToStream(report);
      stream.pipe(res);
    } catch (e) {
      console.log(e);
    }
  } else {
    res.setHeader("Allow", "POST");
    res.status(405).end("Method Not Allowed");
  }
};

export default getInvoices;
