import { stripe } from "../../utils/initStripe";
import { supabaseAdminTM } from "../../utils/initSupabaseAdminTM";

const { Pool, Client } = require("pg");

const client = new Pool({
  user: "postgres.jnurxezspleufiooyekw",
  host: "aws-0-sa-east-1.pooler.supabase.com",
  database: "postgres",
  password: process.env.SUPABASE_POSTGRES_PASSWORD,
  port: 5432,
});

const createTrial = async (req, res) => {
  if (req.method === "POST") {
    console.log(req.body);
    const { email, credits } = req.body;

    console.log("Adicionando", credits, " para email ", email);

    try {
      const { rows } = await client.query(
        `SELECT id FROM auth.users WHERE auth.users.email = '${email}'`
      );

      if (!rows.length) throw { message: "User Not found" };

      const { id } = rows[0];

      await addCreditToCustomer(id, credits);

      return res.status(200).json({ info: "Cliente criado com sucesso" });
    } catch (err) {
      console.log(err?.raw?.message || err.message);

      return res.status(500).json({
        info: err?.raw?.message || err.message,
      });
    }
  } else {
    res.setHeader("Allow", "POST");
    res.status(405).end("Method Not Allowed");
  }
};

const addCreditToCustomer = async (supabaseId, quantity) => {
  if (!supabaseId) throw "No user found";
  // Get customer's UUID from mapping table.
  console.log("Procurando customer para adicionar crédito");
  const uuid = supabaseId;

  const { data, error } = await supabaseAdminTM
    .from("credits")
    .select("current_amount, historical_amount")
    .eq("user_id", uuid)
    .single();
  if (error) {
    //caso onde nunca foi adicionado crédito ao cliente

    console.log("Criando linha com creditos:", quantity, "uuid", uuid);
    console.log(
      "enviando:",
      JSON.stringify({
        user_id: uuid,
        current_amount: parseInt(quantity),
        historical_amount: parseInt(quantity),
        last_purchase: new Date(),
      })
    );
    const { data: newInfo, error: newError } = await supabaseAdminTM
      .from("credits")
      .insert({
        user_id: uuid,
        current_amount: parseInt(quantity),
        historical_amount: parseInt(quantity),
        last_purchase: new Date(),
      });
    console.log("info", newInfo);
    console.log("erro", newError);
  } else {
    console.log(data);
    const currentCredits = data.current_amount + parseInt(quantity);
    const historicalCredits = data.historical_amount + parseInt(quantity);
    const { error } = await supabaseAdminTM
      .from("credits")
      .update({
        current_amount: currentCredits,
        historical_amount: historicalCredits,
        last_purchase: new Date(),
      })
      .eq("user_id", uuid);
    if (error) {
      console.log("something went wrong adding", quantity, "credits to", uuid);
    } else {
      console.log(
        "added",
        quantity,
        "credits to",
        uuid,
        ". He now has:",
        currentCredits
      );
    }
  }

  return true;
};

export default createTrial;
