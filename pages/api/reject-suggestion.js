import axios from "axios";


const acceptSuggestion = async (req, res) => {
  if (req.method === "PUT") {
    const { 
        id_suggestion} = req.body;

    if(!id_suggestion) throw new Error("Missing fields");

    try {
      const TIP_MANAGER_TOKEN = process.env.TIP_MANAGER_TOKEN;
      const { data } = await axios.put(
        `https://api.tipmanager.net:443/v1/suggestions/reject`,
        { id_suggestion: parseInt(id_suggestion)},
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${TIP_MANAGER_TOKEN}`,
          },
        }
      );

      console.log(data)
      return res.status(200).json(data);
    } catch (err) {
        console.log(err);
      return res.status(500).json({
        info: err?.raw?.message || err.message,
      });
    }
  } else {
    res.setHeader("Allow", "PUT");
    res.status(405).end("Method Not Allowed");
  }
};

export default acceptSuggestion;
