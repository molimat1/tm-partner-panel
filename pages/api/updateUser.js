import { supabaseAdmin } from "../../utils/initSupabaseAdmin";

const setUserName = async (req, res) => {
    if (req.method === "POST") {
        const userId = req.body.token;

        try {
            const { error } = await supabaseAdmin.from("users").update(req.body.data).eq("id", userId);

            if (error) {
                console.log(error);
                throw error;
            } else {
                res.status(200).end("User updated");
            }
        } catch (err) {
            console.log(err);
            res.status(500).json({
                error: { statusCode: 500, message: err.message },
            });
        }
    } else {
        res.setHeader("Allow", "POST");
        res.status(405).end("Method Not Allowed");
    }
};

export default setUserName;
