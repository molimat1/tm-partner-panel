import { supabaseAdminTM } from "../../utils/initSupabaseAdminTM";

const getUserData = async (req, res) => {
  try {
    if (req.method === "POST") {
      if (!req?.body?.email) throw new Error("Email não informado");
      const { data, error: errorGettingId } = await supabaseAdminTM
        .rpc("get_user_id_by_email", {
          email: req?.body?.email.toLowerCase(),
        })
        .single();

      if (errorGettingId)
        throw new Error("Problema pegando ID, e-mail deve ser inexistente");

      const { data: user, error } =
        await supabaseAdminTM.auth.api.updateUserById(data.id, {
          password: req?.body?.password,
        });

      if (error) throw new Error("Problema gerando link");

      return res.status(200).json({ data: user });
    } else {
      res.setHeader("Allow", "POST");
      res.status(405).end("Method Not Allowed");
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: error.message });
  }
};

export default getUserData;
