import { supabaseAdminTM } from "../../utils/initSupabaseAdminTM";

const getUserData = async (req, res) => {
  if (req.method === "GET") {
    const { data: user, error } = await supabaseAdminTM.auth.api.resetPasswordForEmail(
      req?.query?.email
    )

    if(error) throw new Error("Problema gerando link")

    return res.status(200).json({data: user})
   
  } else {
    res.setHeader("Allow", "GET");
    res.status(405).end("Method Not Allowed");
  }
};

export default getUserData;
