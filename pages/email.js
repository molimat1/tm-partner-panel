import React, { useState } from "react";
import { getLayout } from "../components/SiteLayout";
import {
  Box,
  Grid,
  Paper,
  Typography,
  TextField,
  styled,
} from "@material-ui/core";
import axios from "axios";
import useSWR from "swr";
import EmailGraph from "../components/Email/emailChart";
import { format, formatDistance } from "date-fns";

import Tooltip from "@material-ui/core/Tooltip";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

const fetcher = (url) =>
  axios
    .get(url)
    .then((res) => res.data)
    .catch((e) => console.warn(e.message));

const StyledTextField = styled(TextField)(({ theme }) => ({
  "& .MuiOutlinedInput-root": {
    backgroundColor: theme.palette.grey[100],
  },
  "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
    borderColor: theme.palette.success.main,
  },
  "& .MuiOutlinedInput-root.Mui-focused": {
    backgroundColor: theme.palette.background.paper,
  },
}));

function Email() {
  const handleClick = (text) => {
    navigator.clipboard.writeText(text);
    alert("Copied to clipboard");
  };

  const { data, mutate, isValidating } = useSWR(
    `https://mail.tipmanager.link:8081/log`,
    fetcher
  );

  const { data: emailData } = useSWR(
    `https://mail.tipmanager.link:8081/log_by_day`,
    fetcher
  );

  const [searchQuery, setSearchQuery] = useState("");

  if (!data) {
    return <div>Loading...</div>;
  }

  if (!data.mails) {
    return "No mails";
  }

  const filteredData = data.mails.filter(
    (mail) =>
      mail.to.toLowerCase().includes(searchQuery.toLowerCase()) ||
      mail.subject.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const handleSearchChange = (event) => {
    setSearchQuery(event.target.value);
  };

  return (
    <Grid
      container
      style={{ overflow: "hidden", paddingLeft: 20, paddingRight: 20 }}
    >
      <Grid item xs={12}>
        <EmailGraph emails={emailData} oldEmails={data.mails} />
      </Grid>
      <Grid item xs={12}>
        <StyledTextField
          placeholder="Search"
          variant="outlined"
          value={searchQuery}
          onChange={handleSearchChange}
          fullWidth
          margin="normal"
        />
      </Grid>
      <Grid item xs={12}>
        {filteredData.reverse().map((i, k) => (
          <Paper
            key={k}
            display={"flex"}
            style={{
              display: "flex",
              marginTop: 10,
              padding: 10,
              justifyContent: "space-between",
              alignItems: "center",
              flexWrap: "wrap",
            }}
          >
            <Typography style={{ width: 80, fontSize: 12 }} noWrap>
              {formatDistance(i.sent_time * 1000, new Date())}
            </Typography>
            <Box width={250}>
              <Typography
                style={{ fontSize: 12, cursor: "pointer" }}
                noWrap
                onClick={() => handleClick(i.to)}
              >
                {i.to}
              </Typography>
            </Box>
            <Box width={200}>
              <Typography style={{ fontSize: 12 }} noWrap>
                {i.subject}
              </Typography>
            </Box>
            <Box
              width={20}
              style={{
                backgroundColor: "gray",
                color: "white",
                padding: 5,
                borderRadius: 5,
                fontSize: 12,
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              {i.tries}
            </Box>
            <Box
              width={50}
              style={{
                backgroundColor: i.status !== "OK" ? "red" : "green",
                color: "white",
                padding: 5,
                borderRadius: 5,
                fontSize: 12,
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              {i.status}
            </Box>
            <Box width={100}>
              <Typography color="error" style={{ fontSize: 12 }}>
                {i.err_str ? i.err_str : ""}
              </Typography>
            </Box>
          </Paper>
        ))}
      </Grid>
    </Grid>
  );
}

Email.getLayout = getLayout;
export default Email;
