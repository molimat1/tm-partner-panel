import React from "react";

import { UserContextProvider } from "../components/hooks/UserContext";
import { DataProvider } from "../components/hooks/DataContext";
import CssBaseline from "@material-ui/core/CssBaseline";
import { ThemeProvider } from "@material-ui/core/styles";
import { supabase } from "../utils/initSupabase";
import "./../style.css";

import theme from "../styles/theme";

export default function MyApp({ Component, pageProps }) {
    const getLayout = Component.getLayout || ((page) => page);
    React.useEffect(() => {
        // Remove the server-side injected CSS.
        const jssStyles = document.querySelector("#jss-server-side");
        if (jssStyles) {
            jssStyles.parentElement.removeChild(jssStyles);
        }
    }, []);
    return (
        <main className={"dark"}>
            <ThemeProvider theme={theme}>
                <UserContextProvider>
                    <DataProvider>
                        <CssBaseline />
                        {getLayout(<Component {...pageProps} />)}
                    </DataProvider>
                </UserContextProvider>
            </ThemeProvider>
        </main>
    );
}
