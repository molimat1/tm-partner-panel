import React, { useEffect } from "react";
import Link from "next/link";
import { Card, Typography, Space } from "@material-ui/core/";
import { supabase } from "../utils/initSupabase";
import { getLayout } from "../components/SiteLayout";
import { useUser } from "../components/hooks/UserContext";
import { useGlobalData } from "../components/hooks/DataContext";
import { makeStyles, CircularProgress } from "@material-ui/core";
import useSWR from "swr";

import PaymentsTable from "../components/PaymentsTable";

const useStyles = makeStyles((theme) => ({
    root: {
        position: "relative",
        zIndex: 1,
        padding: theme.spacing(2),
    },
    spin: {
        height: "90vh",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
    },
}));

const columns = ["Invoice ID", "Cliente", "E-mail", "Data", "Pago", "Valor"];

function Payments() {
    const { invoices } = useGlobalData();

    const classes = useStyles();

    if (!invoices)
        return (
            <div className={classes.spin}>
                <CircularProgress color="secondary" />{" "}
                <Typography variant="h6" style={{ marginTop: 20 }}>
                    Carregando histórico de pagamentos
                </Typography>
            </div>
        );

    return (
        <div className={classes.root}>
            <PaymentsTable data={invoices} columns={columns} />
        </div>
    );
}

Payments.getLayout = getLayout;

export default Payments;
