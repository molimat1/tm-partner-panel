import useSWR from "swr";

import { supabase } from "../utils/initSupabase";
import { useEffect, useState } from "react";
import { useUser } from "../components/hooks/UserContext";

import LoginForm from "../components/LoginForm";
import Loading from "../components/ui/Loading";
import { useRouter } from "next/router";

const Index = () => {
    const router = useRouter();
    const { user } = useUser();
    const query = getQueryStringParams(router.asPath);

    const [authView, setAuthView] = useState("sign_in");

    useEffect(() => {
        const { data: authListener } = supabase.auth.onAuthStateChange(
            (event, session) => {
                if (event === "USER_UPDATED")
                    setTimeout(() => setAuthView("sign_in"), 1000);
                // Send session to /api/auth route to set the auth cookie.
                // NOTE: this is only needed if you're doing SSR (getServerSideProps)!
                fetch("/api/auth", {
                    method: "POST",
                    headers: new Headers({
                        "Content-Type": "application/json",
                    }),
                    credentials: "same-origin",
                    body: JSON.stringify({ event, session }),
                }).then((res) => res.json());
            }
        );

        if (query["type"] === "recovery") {
            router.replace(
                `/password-reset?access_token=${query["/#access_token"]}`
            );
        }

        return () => {
            authListener.unsubscribe();
        };
    }, []);

    useEffect(() => {
        if (user && query["type"] !== "recovery") router.push("/dashboard");
    }, [user]);

    if (!user && query["type"] !== "recovery") {
        return (
            <div
                style={{
                    height: "100vh",
                    backgroundColor: "#222",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    paddingBottom: "10vh",
                }}
            >
                <LoginForm />
            </div>
        );
    }

    return <Loading />;
};

export default Index;

const getQueryStringParams = (query) => {
    return query
        ? (/^[?#]/.test(query) ? query.slice(1) : query)
              .split("&")
              .reduce((params, param) => {
                  let [key, value] = param.split("=");
                  params[key] = value
                      ? decodeURIComponent(value.replace(/\+/g, " "))
                      : "";
                  return params;
              }, {})
        : {};
};
