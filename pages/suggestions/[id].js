import React, { useEffect, useState } from "react";

import { getLayout } from "../../components/SiteLayout";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { useRouter } from "next/router";

import {
  Box,
  Button,
  Modal,
  Paper,
  TextField,
  Typography,
  useMediaQuery,
} from "@material-ui/core/";
import useSWR from "swr";
import axios from "axios";

const fetcher = (url) =>
  axios
    .get(url)
    .then((res) => res.data)
    .catch((e) => console.warn(e.message));

const useStyles = makeStyles((theme) => ({
  root: {
    padding: 10,
    minHeight: "93vh",
  },
  paper: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#454545",
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    height: 500,
  },
}));

function Suggestions() {
  const classes = useStyles();

  const router = useRouter();

  const theme = useTheme();
  const mdUp = useMediaQuery(theme.breakpoints.up("md"));

  const [message, setMessage] = useState("");
  const [loading, setLoading] = useState(false);
  const [titlePT, setTitlePT] = useState("");
  const [descriptionPT, setDescriptionPT] = useState("");
  const [titleEN, setTitleEN] = useState("");
  const [descriptionEN, setDescriptionEN] = useState("");

  useEffect(() => {
    if (!router.query.id && router.isReady) router.back();
    setTitlePT(router.query.title);
    setDescriptionPT(router.query.description);
  }, [router]);

  const handleApprove = async () => {
    setLoading(true);
    try {
      setMessage("");
      if (!titlePT || !descriptionPT || !titleEN || !descriptionEN) {
        setMessage("Preencha todos os campos");
        setLoading(false);
        return;
      }
      console.log("AQUI")
      const response = await axios.put(
        "/api/accept-suggestion",
        {
          id_suggestion: router.query.id,
          title_en: titleEN,
          title_pt: titlePT,
          description_en: descriptionEN,
          description_pt: descriptionPT,
        },
      );
console.log("AQUI 2")
      if (response.status === 200) {
        router.back();
      } else {
        setMessage("erro enviando sugestao para os servidores");
      }
    } catch (e) {
      console.log(e);
    }
    setLoading(false);
  };

  return (
    <div className={classes.root}>
      <Box container spacing={2} px={2}>
        <Typography style={{ color: "white" }} variant="h5">
          Aprovando sugestão
        </Typography>
        <Box my={1} display="flex" flexDirection={"column"} style={{ gap: 10 }}>
          <Typography style={{ color: "white", fontSize: 14, fontWeight: 600 }}>
            Português
          </Typography>
          <TextField
            value={titlePT}
            variant="outlined"
            fullWidth
            inputProps={{
              style: { fontSize: 14, color: "white" },
            }} // font size of input text
            onChange={(e) => setTitlePT(e.target.value)}
            InputLabelProps={{ style: { fontSize: 14, color: "white" } }}
          />
          <TextField
            value={descriptionPT}
            variant="outlined"
            fullWidth
            multiline
            rows={3}
            inputProps={{
              style: { fontSize: 14, color: "white" },
            }} // font size of input text
            onChange={(e) => setDescriptionPT(e.target.value)}
            InputLabelProps={{ style: { fontSize: 14, color: "white" } }}
          />
        </Box>
        <Box my={1} display="flex" flexDirection={"column"} style={{ gap: 10 }}>
          <Typography style={{ color: "white", fontSize: 14, fontWeight: 600 }}>
            Inglês
          </Typography>
          <TextField
            value={titleEN}
            variant="outlined"
            fullWidth
            inputProps={{
              style: { fontSize: 14, color: "white" },
            }} // font size of input text
            onChange={(e) => setTitleEN(e.target.value)}
            InputLabelProps={{ style: { fontSize: 14, color: "white" } }}
          />
          <TextField
            value={descriptionEN}
            variant="outlined"
            fullWidth
            multiline
            rows={3}
            inputProps={{
              style: { fontSize: 14, color: "white" },
            }} // font size of input text
            onChange={(e) => setDescriptionEN(e.target.value)}
            InputLabelProps={{ style: { fontSize: 14, color: "white" } }}
          />
        </Box>
        <Box display="flex" flexDirection={"column"} alignItems="center" my={2}>
          <Box height={50}>
            <Typography variant="caption" color="error">
              {message}
            </Typography>
          </Box>
          <Button
            variant="contained"
            style={{ backgroundColor: "#0c9" }}
            onClick={handleApprove}
            disabled={loading}
          >
            Aprovar
          </Button>
          <Button
            onClick={() => router.back()}
            style={{ color: "red", marginTop: 10 }}
            disabled={loading}
          >
            Voltar
          </Button>
        </Box>
      </Box>
    </div>
  );
}

Suggestions.getLayout = getLayout;

export default Suggestions;
