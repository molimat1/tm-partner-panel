import React, { useEffect, useState } from "react";

import { getLayout } from "../../components/SiteLayout";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { useRouter } from "next/router";

import { Box, Button, Modal, Paper, Typography, useMediaQuery } from "@material-ui/core/";
import useSWR from "swr";
import axios from "axios";



const fetcher = (url) =>
  axios
    .get(url)
    .then((res) => res.data)
    .catch((e) => console.warn(e.message));

const useStyles = makeStyles((theme) => ({
  root: {
    padding: 10,
    minHeight: "93vh",
  },
  paper: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#454545",
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    height: 500,
  },
}));

//Create suggestion item
function SuggestionItem({ data ,mutate}) {
  const router = useRouter();

 const [loading, setLoading] = useState(false);
  const theme = useTheme();
  const mdUp = useMediaQuery(theme.breakpoints.up("md"));

  const handleApprove = () => {
    console.log("approve");
    router.push(`/suggestions/${data.id_suggestion}?title=${data.title}&description=${data.description}`);
  }
  const handleDelete = async () => {
    setLoading(true);
    try {
     
      const response = await axios.put(
        "/api/reject-suggestion",
        {
          id_suggestion: data.id_suggestion,
         
        },
      );

      if (response.status === 200) {
        mutate()
      } else {
        console.log("erro enviando sugestao para os servidores");
      }
    } catch (e) {
      console.log(e);
    }
    setLoading(false);
  };


  return (
    <Paper style={{ padding: 10, backgroundColor: "#444", marginBottom: 10, display: "flex", justifyContent: "space-between", flexDirection: mdUp ? "row" : "column"}}>
      <Box>

      <Typography style={{ color: "white", fontSize: 14, fontWeight: 600}} variant="h6">{data.title}</Typography>
      <Typography style={{ color: "#ccc", fontSize: 14}} variant="h6">{data.description}</Typography>

      </Box>
      <Box display="flex" gap={1} justifyContent={"center"}>
        <Button  style={{color: "#0c9"}} onClick={handleApprove} disabled={loading}>Aprovar</Button>
        <Button  style={{color: "#c03"}} onClick={handleDelete} disabled={loading}>Excluir</Button>
      </Box>
   
    </Paper>
  )
}


function Suggestions() {
  const classes = useStyles();

  const {
    data: dataFetched,
    error: errorFetching,
    mutate,
    isValidating,
  } = useSWR(
    `https://api.tipmanager.net:443/v1/suggestions/waiting_approval`,
    fetcher
  );
  console.log(dataFetched);



  return (
    <div className={classes.root}>
      <Box container spacing={2} px={2}>
      <Typography style={{ color: "white"}} variant="h5">Sugestões recebidas</Typography>
      <Box my={1}>
        {dataFetched?.filter?.(i => !i.analyzed).map((suggestion, key) => <SuggestionItem key={key} data={suggestion} mutate={mutate}/>)}
      </Box>
      </Box>
    </div>
  );
}

Suggestions.getLayout = getLayout;

export default Suggestions;
