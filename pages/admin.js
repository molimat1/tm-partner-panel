import React, { useEffect } from "react";
import { supabase } from "../utils/initSupabase";
import { getLayout } from "../components/SiteLayout";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

import NewUserForm from "../components/NewUserForm";
import VerifyCharge from "../components/VerifyCharge";
import TrialInfo from "../components/TrialInfo";
import TrialInfoStripe from "../components/TrialInfoStripe";
import ExportPayments from "../components/ExportPayments";

import { Typography } from "@material-ui/core/";
import EmailTemplate from "../components/EmailTemplate";
import AddCredits from "../components/AddCredits";
import RedefinePass from "../components/DefinirSenha";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: 10,
    minHeight: "93vh",
  },
  paper: {
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#454545",
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    height: 500,
  },
}));

function Admin() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {" "}
      <Grid container spacing={2}>
        {/* <Grid item xs={12} md={4}>
          <Paper className={classes.paper}>
            <Typography variant="h5" style={{ color: "white" }}>
              Criar usuário
            </Typography>
            <NewUserForm />
          </Paper>
        </Grid> */}
        <Grid item xs={12} md={8}>
          <Paper className={classes.paper}>
            <Typography variant="h5" style={{ color: "white" }}>
              Pegar info de usuário
            </Typography>
            <VerifyCharge />
          </Paper>
        </Grid>
        <Grid item xs={12} md={4}>
          <Paper className={classes.paper}>
            <Typography variant="h5" style={{ color: "white" }}>
              Adicionar créditos
            </Typography>
            <AddCredits />
          </Paper>
        </Grid>

        <Grid item xs={12} md={4}>
          <Paper className={classes.paper}>
            <Typography variant="h5" style={{ color: "white" }}>
              STRIPE Trial
            </Typography>
            <TrialInfoStripe />
          </Paper>
        </Grid>
        <Grid item xs={12} md={4}>
          <Paper className={classes.paper}>
            <Typography variant="h5" style={{ color: "white" }}>
              Exportar pagamentos
            </Typography>
            <ExportPayments />
          </Paper>
        </Grid>
        <Grid item xs={12} md={4}>
          <Paper className={classes.paper}>
            <Typography variant="h5" style={{ color: "white" }}>
              Enviar e-mail template
            </Typography>
            <EmailTemplate />
          </Paper>
        </Grid>
        <Grid item xs={12} md={4}>
          <Paper className={classes.paper}>
            <Typography variant="h5" style={{ color: "white" }}>
              Definir senha
            </Typography>
            <RedefinePass />
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}

Admin.getLayout = getLayout;

export default Admin;
