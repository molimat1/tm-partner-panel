import React, { useEffect } from "react";
import Link from "next/link";
import { Card, Typography, Space } from "@material-ui/core/";
import { supabase } from "../utils/initSupabase";
import { getLayout } from "../components/SiteLayout";
import Grid from "@material-ui/core/Grid";

import ClientCard from "../components/ClientCard";

import { useGlobalData } from "../components/hooks/DataContext";

function Clients() {
    const { customers } = useGlobalData();

    return (
        <div
            style={{
                minHeight: "90vh",
                width: "99%",
                padding: 20,
                flexGrow: 1,
            }}
        >
            <Grid container spacing={3}>
                {Object.entries(customers).map((value, key) => {
                    return (
                        <Grid item xs={12} md={6} lg={4} xl={3}  key={key} >
                            <ClientCard data={value[1]}/>{" "}
                        </Grid>
                    );
                })}
            </Grid>
        </div>
    );
}

Clients.getLayout = getLayout;

export default Clients;
