import React, { useState } from "react";
import Head from "next/head";

import { makeStyles } from "@material-ui/core/styles";
import {
    FormControl,
    TextField,
    Button,
    Typography,
    LinearProgress,
    Paper,
} from "@material-ui/core";

import Logo from "../components/ui/Logo";
import { useUser } from "../components/hooks/UserContext";

import theme from "../styles/theme";
import { useRouter } from "next/router";

const useStyles = makeStyles((theme) => ({
    root: {
        minHeight: "100vh",
        maxWidth: "100vw",
        backgroundColor: "#000",
    },
    content: {
        display: "flex",
        flexDirection: "column",
        background: "linear-gradient(to bottom,#44444422, #00c09055)",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        padding: theme.spacing(4),
    },

    paper: {
        display: "flex",
        flexDirection: "column",
        padding: theme.spacing(3, 0, 0),
        margin: theme.spacing(3, 3, 0),
    },
}));

const initialFValues = {
    password: "",
    passwordValidation: "",
};

export default function Home() {
    const router = useRouter();

    const classes = useStyles();
    const { setNewPass, signOut } = useUser();

    const [values, setValues] = useState(initialFValues);
    const [errors, setErrors] = useState({});
    const [loading, setLoading] = useState(false);
    const [successful, setSuccessful] = useState("");
    const [message, setMessage] = useState("");

    const resetPass = async () => {
        setLoading(true);
        setMessage("");

        const { error, data } = await setNewPass(
            router.query.access_token,
            values.password
        );

        if (error) {
            resetForm();
            setMessage(
                "Problema na redefinição de senha, tente novamente com um novo link. "
            );
            setTimeout(function () {
                router.replace("/");
            }, 2000);
        } else {
            setSuccessful("primary");
            if (data) {
                resetForm();
                setMessage("Ok! você será redirecionado em alguns segundos.");

                setTimeout(function () {
                    router.replace("/dashboard");
                }, 2000);
            }
        }
        setLoading(false);
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setValues({
            ...values,
            [name]: value,
        });
    };

    const validate = (fieldValues = values) => {
        let temp = { ...errors };
        if ("password" in fieldValues) {
            temp.password =
                fieldValues.password.length > 7
                    ? ""
                    : "Sua senha deve ter pelo menos 8 caracteres.";
        }
        if ("passwordValidation" in fieldValues) {
            temp.passwordValidation =
                fieldValues.passwordValidation != fieldValues.password
                    ? "Suas senhas não estão iguais."
                    : "";
        }
        setErrors({
            ...temp,
        });

        if (fieldValues === values)
            return Object.values(temp).every((x) => x == "");
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (validate()) {
            resetPass();
        }
    };

    const resetForm = () => {
        setValues(initialFValues);
        setErrors({});
    };

    return (
        <div className={classes.root} id="home">
            <Head>
                <title key="forgot-password">
                    Portal TM - Redefinição de Senha
                </title>
            </Head>
            <div className={classes.content}>
                <Logo color={"#06D6A0"} width={"25em"} heigth={"60px"} />
                <Typography style={{ color: "white" }} variant="h6">
                    Portal de Parceiros
                </Typography>

                <Paper className={classes.paper} elevation={7}>
                    <Typography
                        variant={"body1"}
                        color={"textSecondary"}
                        align="center"
                    >
                        Escolha uma nova senha
                    </Typography>
                    <FormControl>
                        <form
                            className={classes.form}
                            noValidate
                            onSubmit={handleSubmit}
                        >
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                value={values.password}
                                onChange={handleInputChange}
                                name="password"
                                label="Nova senha"
                                id="password"
                                type="password"
                                error={errors.password}
                                color="primary"
                                helperText={errors.password}
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                value={values.passwordValidation}
                                onChange={handleInputChange}
                                name="passwordValidation"
                                label="Confirme a senha"
                                id="passwordValidation"
                                type="password"
                                error={errors.passwordValidation}
                                helperText={errors.passwordValidation}
                            />
                            <div>
                                <Typography
                                    color={successful}
                                    component="caption"
                                    display="block"
                                >
                                    {message}
                                </Typography>
                            </div>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                style={{
                                    margin: theme.spacing(6, 0, 2),
                                    backgroundColor: "#06D6A0",
                                    color: "#fcfcfc",
                                }}
                                disabled={loading}
                            >
                                Redefinir a senha
                            </Button>
                        </form>
                    </FormControl>
                    {loading && <LinearProgress />}
                </Paper>
            </div>
        </div>
    );
}
