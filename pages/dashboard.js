import React from "react";

import {
  makeStyles,
  CircularProgress,
  Typography,
  Grid,
} from "@material-ui/core";

import { getLayout } from "../components/SiteLayout";
import { useGlobalData } from "../components/hooks/DataContext";
import {
  getWeeklyPayments,
  getMonthValueChart,
  getWeekValueChart,
  getMonthNewSubscribers,
  getAnualInsight,
} from "../utils/paymentFunctions";
import { getWeekDaysArray, getMonthDaysArray } from "../utils/dateHelper";

import ChartSummary from "../components/ChartSummary";
import SalesStatistic from "../components/SalesStatistic";

const useStyles = makeStyles((theme) => ({
  root: {
    position: "relative",
    zIndex: 1,
    padding: theme.spacing(2),
  },
  spin: {
    height: "90vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
}));

function Profile() {
  const { customers } = useGlobalData();
  const classes = useStyles();

  const weekDays = getWeekDaysArray();
  const monthDays = getMonthDaysArray();

  const {
    chartData: weekChartData,
    total: totalWeeklyValue,
    growth: weeklyGrowth,
  } = getWeekValueChart(weekDays, []);

  const {
    chartData: monthSubsChartData,
    total: totalMonthNewSubs,
    growth: monthlySubsGrowth,
  } = getMonthNewSubscribers(monthDays, []);

  const {
    chartData: monthChartData,
    total: totalMonthValue,
    growth: monthlyGrowth,
  } = getMonthValueChart(monthDays, []);

  const {
    chartData: weekChartPayementData,
    total: totalWeeklyPayement,
    growth: weeklyGrowthPayement,
  } = getWeeklyPayments(weekDays, []);

  const anualInsightsData = getAnualInsight([], customers);

  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6} lg={6} xl={3}>
          <ChartSummary
            data={weekChartData}
            color={"#0c9"}
            xAxis={"day"}
            tooltip={"R$"}
            title={`R$ ${totalWeeklyValue}`}
            subtitle={"Valor na Semana"}
            growth={weeklyGrowth}
          />
        </Grid>
        <Grid item xs={12} md={6} lg={6} xl={3}>
          <ChartSummary
            data={weekChartPayementData}
            color={"#9ca"}
            xAxis={"day"}
            tooltip={""}
            title={`${totalWeeklyPayement}`}
            subtitle={"Pagamentos na Semana"}
            growth={weeklyGrowthPayement}
          />
        </Grid>
        <Grid item xs={12} md={6} lg={6} xl={3}>
          <ChartSummary
            data={monthChartData}
            color={"#0c9"}
            xAxis={"day"}
            tooltip={"R$"}
            title={`R$ ${totalMonthValue}`}
            subtitle={"Valor no mês"}
            growth={monthlyGrowth}
          />
        </Grid>
        <Grid item xs={12} md={6} lg={6} xl={3}>
          <ChartSummary
            data={monthSubsChartData}
            color={"#0c9"}
            xAxis={"day"}
            tooltip={""}
            title={`${totalMonthNewSubs}`}
            subtitle={"Novas Assinaturas no Mês"}
            growth={monthlySubsGrowth}
          />
        </Grid>
        <Grid item xs={12}>
          <SalesStatistic data={anualInsightsData} />
        </Grid>
      </Grid>
    </div>
  );
}

Profile.getLayout = getLayout;

export default Profile;
